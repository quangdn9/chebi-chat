<?php
/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 8/3/17
 * Time: 3:08 PM
 */
if (isset($_GET["title"])) {

    $title = $_GET["title"];

} else
    return;


require("./includes/Tiengtrung_conf.php");
require("./includes/hecheng_baidu/AipSpeech.php");

$baidu = new AipSpeech(
    $baiduLang_conf["AppID"],
    $baiduLang_conf["Apikey"],
    $baiduLang_conf["Secret"]
);

/*MAIN FUNC*/
if (isset($_GET["text"])) {


    $text = $_GET["text"];
    $content = trim(strip_tags($text));

    //var_dump($content);

    /*BAIDU*/


    $per = 0;
    $spd = 5;

    if (isset($_GET['per'])) {

        $per = $_GET['per'];
    }

    if (isset($_GET['spd'])) {

        $spd = $_GET['spd'];
    }

    /*BAIDU CHINESE AND ENGLISH*/
    echo $result = $baidu->synthesis($content, 'zh', 1, array(
        'vol' => 5,
        'per' => $per,
        'spd' => $spd
    ));



//    var_dump($result);

    // 识别正确返回语音二进制 错误则返回json 参照下面错误码
    if(!is_array($result)){

//        file_put_contents('audio.mp3', $result);
//        header("Content-Disposition: attachment; filename=audio.mp3");

    }


}


/*
 * 参数	可需	描述
tex	必填	合成的文本，使用UTF-8编码，请注意文本长度必须小于1024字节
lan	必填	语言选择,目前只有中英文混合模式，填写固定值zh
tok	必填	开放平台获取到的开发者 access_token
ctp	必填	客户端类型选择，web端填写固定值1
cuid	必填	用户唯一标识，用来区分用户，计算UV值。建议填写能区分用户的机器 MAC 地址或 IMEI 码，长度为60字符以内。
spd	选填	语速，取值0-9，默认为5中语速
pit	选填	音调，取值0-9，默认为5中语调
vol	选填	音量，取值0-15，默认为5中音量
per	选填	发音人选择, 0为普通女声，1为普通男声，3为情感合成-度逍遥，4为情感合成-度丫丫，默认为普通女声
 * */

//FPT
