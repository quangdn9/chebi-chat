<?php
/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 8/15/17
 * Time: 11:30 AM
 */
// Create connection

require_once __DIR__ . '/chebi/app/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

//if (strpos(ROOT,"tiengtrung") == false) $path = $path."/tiengtrung/";
//echo $path."/includes/Tiengtrung_conf.php";

$db_server = getenv("DB_SERVER");
$db_name = getenv("DB_NAME");
$db_user = getenv("DB_USER");
$db_pass = getenv("DB_PASS");




function connectdb($servername, $username, $password, $dbname)
{

    $conn = new mysqli($servername, $username, $password, $dbname);

    $conn->set_charset('utf8');

//$conn->query('SET NAMES UTF8');

    if ($conn->connect_error) {

        die("Connection failed: " . $conn->connect_error);
    }

    return $conn;
}


$conn = connectdb($db_server, $db_user, $db_pass, $db_name);

?>