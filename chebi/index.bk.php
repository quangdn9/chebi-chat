<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 9/26/2018
 * Time: 12:37 AM
 */


/*
 * Author: Do Nhat Quang
 * Chebi Chat Main Function
 * */
header("Content-type: text/html; charset:utf-8");
//header("Content-type: application/json; charset:utf-8");

define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
require(ROOT . "/db_conn.php");

/*-------CHEBI API------*/
/*CHEBI QUERY CLASS*/
require("api/AipHttpClient.php");
require_once("api/translator.php");


/*CHEBI APP CONTROLLER*/
require_once("app/app_controller.php");

/*sensitive analyze*/
require("api/sensitive.php");

/*CHEBI APP*/
require("chebi_get_data.php");
require("app/Chebi_app.php");

/*SET UP APP CONTROLLER*/
$Chebi = new app_controller();

/*PARAM VALUE*/
$lc = $_GET["lc"];
$keyword = trim(strtolower($_GET["reqText"]));


/*------------*/
if (!isset($_GET["reqText"]) || !isset($_GET["lc"])) {

    return "false";
}

/*FANPAGE ID*/
$pageid = "";
if (isset($_GET["pageid"]) && $_GET["pageid"] != "") {

    $pageid = $_GET["pageid"];
}

/*CHINESE*/
$chinese = false;
if ($lc == "zh" || $lc == "hk") {

    $chinese = true;
}

/*DEV MODE*/
$response = "";
$property = "";

$result = false; /*PRINT THE RESULT*/

try {

    /*DEFINE APP*/
    $ChineseApp = new tiengtrung_app();

    /*CHECK APP KEYWORD*/
    $check_App = $Chebi->checkApp(strtolower($keyword), $lc);


    /*IF SIMSIMI CALL APP TO DISPLAY*/
    if ($check_App) {

        /*CHECK IF APP TYPE (array) -- MINI APP IN LOCAL AS 成语, 食品*/
        if ($check_App["local"] == true) {

            /*FIND RESPONSE FROM LOCAL DATABASE*/
            $ChineseApp->setDbpic($servername, $username, $password, $db_piwigo); /*define conn to piwigo database*/




            /*GET LOCAL */
            $result = get_local_data($check_App, $ChineseApp);

            /*var_dump($check_App);
            var_dump($result);
            exit();*/

        }

        /*CHEBI App*/
        else {

            $Chebi_app = new Chebi_app($result, $lc);

            $result = $Chebi_app->check_app();

        }
    }

    /*IT'S NOT APP TYPE*/
    else {

        $except_word = array("新闻", "娱乐");

        $flag = false;
        foreach ($except_word as $word) {

            if (strpos($keyword, $word) !== false) $flag = true;
        }

        /*NOT INCLUDE EXCEPT WORD -> LOCAL*/
        if ($flag == false) {

            /*CHAT TEXT WITH BOT*/
            $result = $Chebi->getChatResponse($keyword, $pageid, $lc);

            $response = "";
            if ($result) {

                $response = $result->{"reply"};
            }

            $result = array(

                "response" => $response,
                "msg" => "OK",
                "source" => "Chebi from Local"
            );
        }

    }

    /*---------------*/
    /*NO RESULT --> REQUEST FROM SIMSIMI DB*/
    $filter = false;

    /*GET DATA FROM SIMSIMI API*/
    if ($result["response"] == "") {

        /*-----DATA INIT----*/

        /*FILTER SENSITIVE WORDS (BAD WORDS)*/
        /*if ($lc == "vn" || $lc == "en") {

            $sensitive = new Sensitive(($keyword), $conn);
            $filter = $sensitive->filter($lc);

        }*/

        /*Temp varible*/
//        $filter = false;

        /*HAVE BAD WORDS SENSITIVE*/
        /*if ($filter) {

            if ($lc == "zh") {

                $response = "您的输入有敏感词，Chebi无法回答你";

            } else {

                $response = "Từ khoá chứa nội dung nhạy cảm, thông cảm cho Chebi nhé";
            }

            $result = array(

                "response" => $response,
                "msg" => "OK",
                "source" => $filter->source
            );

        } else {*/


        /*DETECT LANGUAGE*/
        /*if ($chinese == false) {

            $lc = detect_lang($keyword);

        }
        */

        /*GET SIMSIMI*/
        $lc = "zh";
        $result = get_remote_chat($keyword, $lc);


    }


    /*ADD PINYIN*/
    if ($chinese && $filter == false && isset($result["property"]) == false) {

        $pinyin = $ChineseApp->convertPinyin($result["response"]);
        $result["pinyin"] = $pinyin;
    }

    /*SET LANG TO FRONT-END*/
    $result["language"] = $lc;


    /*OUTPUT*/
    echo json_encode($result);


} catch (Exception $e) {

    echo $e->getMessage();
}