<?php

$param = array(

    "map_api" => "AIzaSyB_WEPzrCLjKaY9HVHPTdyxLFP3RbnO2Uc"
);

?>

<!--remote CDN JAVASCRIPT LIB-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/js/materialize.min.js"></script>

<!--GOOGLE MAP-->

<!--AIzaSyAgZwpsNaMwDcRG9EaAmmuA9nqsnj_zBz8-->
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=<?php echo $param["map_api"]; ?>&libraries=places,geometry&language=vi">
</script>

<!--<script async defer src="https://apis.google.com/js/api.js"
        onreadystatechange="console.log('Load Google API Finish');">
</script>
-->

<!--NUMBER-->
<!--<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>-->

<!--FIREBASE-->
<!-- Insert these scripts at the bottom of the HTML, but before you use any Firebase services -->

<!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/6.5.0/firebase-app.js"></script>

<!-- Add Firebase products that you want to use -->
<script src="https://www.gstatic.com/firebasejs/6.5.0/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/6.5.0/firebase-firestore.js"></script>

<script src="https://cdn.firebase.com/libs/firebaseui/4.1.0/firebaseui.js"></script>
<link type="text/css" rel="stylesheet" href="https://cdn.firebase.com/libs/firebaseui/4.1.0/firebaseui.css" />

<!--ResponsiveVoice Text To Speech API-->
<script src="https://code.responsivevoice.org/responsivevoice.js"></script>

<!--jsSocials CDN-->
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>

<!--LOCAL JAVASCRIPT LIB-->


<!--GLOBAL TOOLS-->
<script src="./js/tools.js?<?php echo mt_rand(); ?>"></script>
<script src="./js/bindact.js?<?php echo mt_rand(); ?>"></script>


<!--YOUTUBE-->
<script src="https://www.youtube.com/iframe_api"></script>
<script src="./js/google_api/youtube.js?<?php echo mt_rand(); ?>"></script>

<!--WIKI-->
<script src="./js/wiki_api/wiki.js?<?php echo mt_rand(); ?>"></script>

<!--MAP SCRIPT-->
<script src="./js/google_api/map.js?<?php echo mt_rand(); ?>"></script>
<script src="./js/google_api/map_init.js?<?php echo mt_rand(); ?>"></script>



<!----------- auth FILE ------------>

<!--FIREBASE auth-->
<script language="JavaScript" src="./js/Firebase.js?<?php echo mt_rand(); ?>"></script>

<!--CHEBI USER-->
<script src="./js/chebi_user.js?<?php echo mt_rand(); ?>"></script>



<!----------- voice FILE ------------>

<!--TIENG TRUNG-->
<script src="./js/baidu_api/Baidu.js?<?php echo mt_rand(); ?>"></script>

<!--NATURE LANGUAGE-->
<script src="./js/baidu_api/nlp.js?<?php echo mt_rand(); ?>"></script>

<!--VOICE RECORD BY GOOGLE-->
<script language="JavaScript" src="./js/google_api/voice_reco.js?<?php echo mt_rand(); ?>"></script>

<!----------- CHAT SIMSIMI FILE ------------>

<!--CHATTING WITH SAMSIMI-->

<script src="./js/chebichat_func.js?<?php echo mt_rand(); ?>"></script>
<script src="./js/chebichat_app.js?<?php echo mt_rand(); ?>"></script>
<script src="./js/chebichat_send.js?<?php echo mt_rand(); ?>"></script>
<script src="./js/chebichat_training.js?<?php echo mt_rand(); ?>"></script>


<!--PAGE JS-->
<script src="./js/pages/pages_main.js?<?php echo mt_rand(); ?>"></script>

<!--MAIN LOADING-->
<script language="JavaScript" src="./js/chebichat_main.js?<?php echo mt_rand(); ?>"></script>