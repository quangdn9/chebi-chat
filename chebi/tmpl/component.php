<!--MP3-->
<div id="mp3" style="display: none;">
    <!--oncanplaythrough="Baidu.AudioLoaded();"-->
    <audio id="mp3_audio" onended="Baidu.endSound();" controls AUTOPLAY>
        <source src="" type="audio/mpeg">
        Your browser does not support the audio element
    </audio>

</div>

<!--OUTPUT CONSOLE-->
<div id="output" class="row col s12"></div>

<!--SETTING MODAL-->
<div id="setting_modal" class="modal">
    <div class="modal-content">
        <div class="row">
            <div class="col s12 orange" style="color: white; text-align: center;">

                <span class="flow-text">Bảng cấu hình</span>


            </div>
        </div>


        <!--CHANGE LANGUAGE-->
        <form class="col s12">
            <div style="" class="row">


                <!--SELECT VOICE AND CHAT LANG-->
                <div class="input-field col s12 m6">
                    <select class="icons" id="lang_select">
                        <option value="" disabled>Chọn ngôn ngữ nhập</option>
                        <option id="lang_zh" value="cmn-Hans-CN" data-icon="./flags/4x3/cn.svg" class="left circle">中文(简体)</option>
                        <option id="lang_hk" value="yue-Hant-HK" data-icon="./flags/4x3/hk.svg" class="left circle">中文(繁體)</option>
                        <option id="lang_ja" value="ja-JP" data-icon="./flags/4x3/jp.svg" class="left circle">日本語</option>

                        <option id="lang_fr" value="fr-FR" data-icon="./flags/4x3/fr.svg" class="left circle">France</option>

                        <option id="lang_en" value="en-US" data-icon="./flags/4x3/gb.svg" class="left circle">English</option>
                        <option id="lang_vn" value="vi-VN" data-icon="./flags/4x3/vn.svg" class="left circle" selected="selected">Tiếng Việt</option>
                    </select>
                    <label>Chọn ngôn ngữ</label>
                </div>

                <!--SELECT TRANSLATE LANG-->
                <div class="input-field col s12 m6">
                    <select class="icons" id="translate_select">
                        <option value="" disabled>Chọn ngôn ngữ dịch</option>
                        <option id="translate_zh" value="cmn-Hans-CN" data-icon="./flags/4x3/cn.svg" class="left circle" selected="selected">中文(简体)</option>
                        <option id="translate_hk" value="yue-Hant-HK" data-icon="./flags/4x3/hk.svg" class="left circle">中文(繁體)</option>
                        <option id="translate_ja" value="ja-JP" data-icon="./flags/4x3/jp.svg" class="left circle">日本語</option>

                        <option id="translate_fr" value="fr-FR" data-icon="./flags/4x3/fr.svg" class="left circle">France</option>

                        <option id="translate_en" value="en-US" data-icon="./flags/4x3/gb.svg" class="left circle">English</option>
                        <option id="translate_vn" value="vi-VN" data-icon="./flags/4x3/vn.svg" class="left circle" >Tiếng Việt</option>
                    </select>
                    <label>Chọn ngôn ngữ</label>
                </div>



            </div>

            <!--BACKGROUND-->
            <div style="" class="row">

                <!--SELECT THEME-->
                <div class="input-field col s12 m6">
                    <select id="theme_select">
                        <option value="" disabled>Chọn chủ đề</option>
                        <option value="chebi" selected="selected">Chebi Theme</option>
                        <option value="female">Mỹ nữ</option>

                        <option value="personal">Ảnh đã chọn</option>

                    </select>
                    <label>Chọn chủ đề</label>
                </div>


                <!--SELECT VOICE STYLE-->
                <div class="input-field col s12 m6">


                    <select id="voice_select">
                        <optgroup label="Giọng nam">
                            <option value="2">Giọng nam thường</option>
                            <option value="3">Giọng diễn cảm</option>
                        </optgroup>
                        <optgroup label="Giọng nữ">
                            <option value="0">Giọng nữ thường</option>
                            <option value="4" selected>Giọng nhí nhảnh</option>
                        </optgroup>
                    </select>
                    <label>Chọn giọng nói</label>


                </div>

            </div>


            <!--CHANGE RANGER LEVEL-->
            <div style="" class="row">

                <!--BAD WORD LEVEL-->
                <div class="input-field col s12 m6">
                    <p class="range-field">
                        <input type="range" id="badwords" min="0" max="10" value="0"/>
                    </p>
                    <label>Lọc từ nhạy </label>
                </div>

                <!--SPEED-->
                <div class="input-field col s12 m6">
                    <p class="range-field">
                        <input type="range" id="speeds" min="0" max="9" value="4"/>
                    </p>
                    <label>Tốc độ đọc</label>
                </div>

            </div>

        </form>


        <!--USER BUTTON-->
        <div id="user_btn_wrap" style="display: none;" class="center">
            <a class="waves-effect waves-light btn orange center" onclick="Firebase.writeUserData();">Lưu trữ</a>
            <a class="waves-effect waves-light btn orange center" onclick="Firebase.logout();">Đăng xuất</a>
        </div>

        <!--USER BUTTON-->
        <div id="anonymous_btn_wrap" style="" class="center">
            <a class="waves-effect waves-light btn orange center" onclick="Firebase.openLogin();">Đăng nhập</a>
        </div>


    </div>

    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Đóng</a>
    </div>
</div>






