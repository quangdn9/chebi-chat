<!--USER LOGIN MODAL-->
<div id="login_modal" class="modal modal-fixed-footer">
    <div class="modal-content center">

        <img src="/assets/chebichat_design/avatar_icon_2.png" width="200" alt="" class="large" style="">

        <!--        <span style='clear:both'>Đăng nhập</span><br>-->

        <div id="firebaseui-auth-container"></div>
    </div>

    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Đóng</a>
    </div>

</div>

<!--MAP MODAL-->

<!--GOOGLE MAP-->
<input id="pac-input" class="controls" type="text" placeholder="Search Box" value="cafe"
       style="width: 50%; opacity: 0.7; background: rgba(255,255,255,0.7); display: none;">

<!-- Modal Structure -->
<div id="map_btn_wrap" style="display: none;">
    <a class="waves-effect waves-light btn orange" onclick="Gmap.events('fullscreen');"><i class="material-icons left">fullscreen</i>Fullscreen</a>
</div>


<!--GOOGLE MAP-->
<div id="map" style="height: 300px;"></div>


<!--YOUTUBE-->
<div id="youtubeModal" class="modal modal-fixed-footer">
    <div class="modal-content" style="padding: 0px;">
        <!--IFRAME-->
        <!-- 1. The <iframe> (and video player) will replace this <div> tag. -->
        <div id="player"></div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Đóng</a>
    </div>
</div>


<!--WIKI MODAL-->
<div id="wikiModal" class="modal ">
    <div class="modal-content wikiModal_content" style="padding: 0px;">

    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Đóng</a>
    </div>
</div>


<!--training MODAL-->
<div id="trainingModal" class="modal modal-fixed-footer">
    <div class="modal-content" style="padding: 0px;">

        <div class="row">
            <div class="col s12 orange" style="color: white; text-align: center;">

                <span class="flow-text">Huấn luyện Chebi </span>


            </div>
        </div>


        <div class="row">
            <form class="col s12">

                <div class="row">
                    <div class="input-field col s12">
                        <input disabled value="" id="chebi-training-source" type="text" class="validate chebi-training-source">

                    </div>
                </div>


                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Hướng dẫn cho Chebi" id="chebi-training-reply" type="text">
                        <label for="first_name">Hướng dẫn Chebi trả </label>
                    </div>

                </div>

                <div class="row">

                    <button class="btn waves-effect waves-light" type="submit" name="action" onclick="Training.trainingSend(); return false;">Submit
                        <i class="material-icons right">send</i>
                    </button>


                </div>
            </form>
        </div>
    </div>

    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Đóng</a>
    </div>
</div>
