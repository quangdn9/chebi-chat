<!--FLAG-->
<link href="./css/flag-icon.css" rel="stylesheet">



<!--REMOTE CDN CSS FILE LIB-->
<!--font awesome-->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
<!-- Compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/css/materialize.min.css">

<!--FIREBASE auth-->


<!--jsSocials CDN-->
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />

<link rel="stylesheet" href="./css/chat.css?<?php echo mt_rand(); ?>">
<link rel="stylesheet" href="./css/chebi-style.css?<?php echo mt_rand(); ?>">
<link rel="stylesheet" href="./css/wiki.css?<?php echo mt_rand(); ?>">

<style type="text/css">

    .fixed-action-btn.horizontal ul li {
        display: inline-block;
        margin: 10px 5px 0 0;
    }

</style>