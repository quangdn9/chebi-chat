/**
 * Created by donhatquang on 11/13/17.
 */
$.extend(Chat, {

    /*SEND MESSAGE TO CHATBOT CHICKEN*/
    send: function () {

        var reqText = $("#reqText").val();

        /*RESET TEXT BOX*/
        $("#reqText").val("");


        /*CHECK EMPTY*/
        if (reqText.length != 0) {

            /*DETECT LANGUAGE*/
            var chinese = Chat.langDetect(reqText, "chinese");

            if (chinese) {

                /*SET SELECT LANGUAGE*/
                var lang = {
                    voice: "cmn-Hans-CN",
                    chebi: "zh"
                };

                /*SET SELECTOR LANGUAGE*/
                Chebi_user.setSelected("#lang_select", $("#lang_" + lang.chebi));

                changeLanguage(lang.voice);
            }

            /*OTHER LANG*/
            else {

                /*SETTING LANGUAGE DETECT BY BACKEND*/
                /*SET DEFAULT LANGUAGE*/

                var voice = setLangMap("detect");

                /*CHANGE LANGUAGE FOR VOICE*/
                changeLanguage(voice);
            }

            /*------> begin*/

            /*ADD CHAT TEXT TO USER SCREEN*/
            this.addChat('user', reqText);

            //return false;

        } else {

            console.log("Simsimi call: Type text");

            return false;
        }

        /*--------*/

        /*BEGIN SEND DATA*/
        // console.log(lc);
        // return;

        var lc = this.g_language;
        var ft = this.g_filter;

        $.ajax({
            url: "chebi_chat.php",
            data: {
                "lc": lc,
                "ft": ft, /*FILTER*/
                "normalProb": 10,
                "reqText": reqText,
                "pageid": page,

                "devmode": devmode
            },
            type: 'GET',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",

            error: this.errorData,

            /*FEEDBACK DATA*/
            success: this.successData

        });

        return false;
    },

    errorData: function (xhr, textStatus, errorThrown) {
        console.log("error : " + textStatus);

        if (xhr.status == 401) {
            //FirebaseLogin();
        } else {
            if (xhr.responseText == 'DailyLimit') {
                Chat.addChat('simi', 'See you tommorow~');
            } else if (xhr.responseText == 'Not Human') {
                Chat.addChat('simi', 'I think you are not human..');
            } else {
                Chat.addChat('simi', 'I have no response.');
            }
        }
    },

    /*RECEIVE DATA SUCCESSFUL*/
    successData: function (json) {

        console.log("CHAT DATA:");
        console.log(json);

        /*CONTENT*/
        var content = json.response;

        if (content != null) {

            /*SETTING LANGUAGE DETECT BY BACKEND*/
            /*SET SELECT LANGUAGE*/
            var lang = json.language;
            var voice = setLangMap(lang);

            console.log("Current language now: ");
            console.log(lang + "||" + voice);

            /*SET SELECTOR LANGUAGE*/
            Chebi_user.setSelected("#lang_select", $("#lang_" + lang));

            /*CHANGE LANGUAGE FOR VOICE*/
            changeLanguage(voice);


            /*CHANGE VOICE OF LAST CHAT_USER */
            $(".chat_user:last").find(".sim_speak").attr("data-lang", lang);


            /*CHECK PROPERTY*/
            if (json.property) {

                console.log(json.property);

                /*CALL CHAT WITH APP*/
                var appWithText = Chat.chatApp(json.property);

                /*ADD CHAT WITH PROPERTY SPECIAL*/
                /*
                 * IF RETURN FALSE MEAN DON'T NEED CALL ADD CHAT AGAIN, only call add Property;
                 * */
                if (appWithText) {

                    console.log(appWithText);
                    Chat.addChat('simi', content, appWithText);
                }

            }

            /*NORMALLY INPUT CHAT*/
            else {

                if (Chat.g_language == "zh" || Chat.g_language == "hk") {

                    /*FIX NICKNAME*/
                    content = Chat.addNickname(content);

                    /*ADD PINYIN FOR CHINESE*/
                    if (json.pinyin != undefined) {

                        /*SPLIT*/
                        if (content.indexOf("|") != -1) {

                            var data_tmp = content.split("|");

                            var data_pinyin = json.pinyin;
                            data_pinyin = data_pinyin.split("|");

                            console.log(json.pinyin);

                            var text_tmp = "";

                            for (i in data_tmp) {

                                var words = data_tmp[i];
                                text_tmp += Chat.addPinyin(words,data_pinyin[i]);
                                text_tmp += "<br/>";
                            }

                            content = text_tmp;
                        }
                        else
                            content = Chat.addPinyin(content,json.pinyin);
                    }
                }

                /*DISPLAY CHAT*/
                Chat.addChat('simi', content, false);

            }
        }

        /*ERROR - NO REPSONSE*/
        else {

            Chat.addChat('simi', 'Chebi chưa hiểu nội dung này, bạn có thể gợi ý trả lời cho Chebi không?');
        }

        return;
    },


    /*FIX NICKNAME*/
    addNickname: function (content) {

        // console.log(content);

        if (content.indexOf("昵称") != -1) {

            /*USER NAME*/
            if (content.indexOf("发言人昵称") != -1) {

                if (Chebi_user.userinfo != "") {
                    content = content.replace("发言人昵称", Chebi_user.userinfo.displayName);

                }
                else content = content.replace("发言人昵称", Chat_config.nickname);

            }

            /*BOT NAME*/
            else if (content.indexOf("机器人昵称") != -1) {
                content = content.replace("机器人昵称", Chat_config.botname);
            }

            /*FIX SPECIAL*/
            content = content.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');

        }

        return content;
    },


    /*ADD PINYIN*/
    addPinyin: function (content, pinyin) {

        // var pinyin = json.pinyin;
        // console.log(pinyin);

        pinyin = $.trim(pinyin);

        /*ADD RUBY*/
        var chinese_data = content.split("");
        var pinyin_data = pinyin.split(" ");

        console.log(pinyin_data);

        var text = "<ruby>";
        var pinyin_count = 0;

        for (i in chinese_data) {

            var character = chinese_data[i];
            var character_pinyin = pinyin_data[pinyin_count];

            text += character;

            if (Chat.langDetect(character, "chinese") == true) {

                text += "<rt>"+character_pinyin+"</rt>";
                pinyin_count++;
            }
            else {
                text += "<rt>&nbsp;</rt>";
            }
        }

        text += "</ruby>";

        // text += '<br/><span class="pinyin">' + pinyin + '</span>';

        return text;
    },

    /*ADD VOICE*/
    addChatBtn: function (property, who) {

        /*SET UP BUTTON*/
        /*NORMALLY INPUT - NOT PROPERTY STYLE */
        if (!property || property != "finish") {

            var text = "";

            /*MORE PROPERTY FEATURES (SENTIMENT - TRANSLATOR)*/

            text += '  <div style="position: relative; height: 50px;" class="chebi-action-' + who + '">';


            /*MAIN BUTTON*/

            /*SIMI*/
            if (who == "simi") {


                text += '<div class="fixed-action-btn horizontal click-to-toggle chebi-action-btn" style="position:absolute;">    ' +
                    '<a class="btn-floating btn-small orange chebi-toggle-btn">      ' +
                    '<i class="material-icons">more_horiz</i></a>    ';

                text += '<ul style="left: -260px;" class="chebi-chat-btn">';
            }

            /*USER*/
            else {

                text += '<div class="fixed-action-btn horizontal click-to-toggle chebi-action-btn" style="position:absolute;">    ' +
                    '<a class="btn-floating btn-small chebi-toggle-btn">      ' +
                    '<i class="material-icons">more_horiz</i></a>    ';

                text += '<ul class="chebi-chat-btn">';

                /*TRAINING FOR CHEBI*/
                text += '<li><a class="btn-floating training" onclick="Training.trainingModal(this);"><i class="material-icons">school</i></a></li>      ';
            }


            /*VOICE*/
            text += '<li><a class="btn-floating green sim_speak"  data-lang="' + Chat.g_language + '" onclick="Baidu.playSound(this);"><i class="material-icons volume_up_loading">volume_up</i></a></li>      ';

            /*TRANSLATE*/
            text += '<li><a class="btn-floating red g_translate" onclick="Chat.addPropertyChat(\'translator\', this);"> <i class="material-icons">g_translate</i></a></li>      ';

            /*SENTIMENT*/
            text += '<li><a class="btn-floating yellow darken-1 sentiment" onclick="Chat.addPropertyChat(\'sentiment\', this);"><i class="material-icons">insert_emoticon</i></a></li>      ';


            text += '</ul>  </div></div>';


            /*VOICE ICON*/
            $(".working").append(text);

        }

        return;
    },

    /*ADD SHARE social*/
    addShare: function () {
        <!--SHARE LINK-->

        var text = '<div class="shareBox"></div>';

        var keyword = $(".chat_user:last").find(".bubble").text();

        // var host = Chebi_user.bg_host;
        // var background = host + "/" + Chat_config.background;

        var query = {

            keyword: keyword,
            lang: Chat.g_language
            // share: "true"
            // image: background
        };

        console.log(query);

        // query = $.param(query);

        // var url = "http://www.chebichat.com/"+query;
        var url = "http://chebichat.com/" + query.lang + "/" + keyword + ".html";

        console.log(url);

        var param = {
            showLabel: false,
            showCount: false,
            url: url,
            text: "Chebi Chat - Trợ lý ảo của bạn",

            // showCount: "inside",
            shareIn: "popup",
            shares: ["facebook", "googleplus", "email", "twitter", "whatsapp"]
        };


        $(".working .bubble").append(text);

        $(".shareBox").jsSocials(param);

        return;
    }
})
