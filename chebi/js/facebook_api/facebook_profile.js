/**
 * Created by donhatquang on 9/5/17.
 */
var Facebook = {

    //fireBase:
    User: {},
    graphURL: "https://graph.facebook.com/",
    youtubeURL: "https://www.googleapis.com/youtube/v3/",

    init: function () {

        FB.init({
            appId: '119997001994273',
            autoLogAppEvents: true,
            xfbml: true,
            version: 'v2.10',
            status: true
        });
        FB.AppEvents.logPageView();

    },

    checkLogin: function () {

        /*SET FACEBOOK USER*/
        FB.getLoginStatus(function (result) {

            // console.log(result);

            if (result.status == "connected") {

                var auth = result.authResponse;

                Facebook.User = {

                    token: auth.accessToken,
                    // info: user,
                    uid: auth.userID

                }

                console.log(Facebook.User);
            }

            else Firebase.checkStatus();


        })

        return;
    },

    graph: function (request) {

        var app = request.app;
        var param = request.param;

        var url = this.graphURL + app;
        param.access_token = this.User.token;

        $.getJSON(url, param, function (result) {


            console.log(result);
        })

        return;
    }


}