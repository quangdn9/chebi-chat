/**
 * Created by donhatquang on 11/21/17.
 */

/*BIND ACTION*/
var Bindact = function () {

    /*BEGIN*/
    /*----------- */

    /*FORMAT MARERIAL STYLE*/
    $('select').material_select();
    $('.modal').modal();


    /*-----------
     * */
    $("#record").on("click", function (event) {

        Record.startRecording(event);
    });
    $("#recordbtn").on("click", function () {

        Chat.send();
        return false;
    });

    /*SELECT LANGUAGE*/
    $('#lang_select').on("change", function () {

        /*CHANGE VOICE RECORDER LANG*/
        var lang = $(this).val();

        changeLanguage(lang, "voice");

        return;
    });

    /*SELECT LANGUAGE*/
    $('#translate_select').on("change", function () {

        /*CHANGE VOICE RECORDER LANG*/
        var lang = $(this).val();

        changeLanguage(lang,"translate");

        return;
    });

    /*THEME LANGUAGE*/
    $('#theme_select').on("change", function () {

        /*CHANGE VOICE RECORDER LANG*/
        var bg_mode = $(this).val();
        Chat_config.background_mode = bg_mode;

        /*SET THEME*/
        Chebi_user.setTheme();

        return;
    });

    /*SELECT LANGUAGE*/
    $('#voice_select').on("change", function () {

        var voice_type = $(this).val();
        Baidu.voiceConf.per = voice_type;

        console.log("Change baidu voice to: " + voice_type);
    });

    $("#badwords").on("change", function () {

        var badwords = $(this).val();
        Chat.g_normalProb = badwords;

        return;
    })

    $("#speeds").on("change", function () {

        var speeds = $(this).val();
        Baidu.voiceConf.spd = speeds;

        return;
    })


    /*BIND KEY*/
    $("body").on("keyup", function (e) {

        var key = (e.keyCode);

        if (key == 13) {

            Chat.send();
            return false;

        }
        else if (key == 17) { //key == 91

            // $("#record").trigger("click");
            //console.log(key);
        }

        return;

    });

    return;
}
