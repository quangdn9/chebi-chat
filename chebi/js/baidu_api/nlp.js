/**
 * Created by donhatquang on 11/6/17.
 */
var NLP = {

    /*add sentiment*/
    sentiment: function (keyword, render) {

        var url = "/chebi/api/review_tags.php";
        var option = {

            act: "sentiment",
            keyword: keyword,
            devmode: devmode,
            lang: Chat.g_language
        };

        /*CHECK LENGTH*/
        //WHEN CONTENT LONG MORE THAN 10, CHECK SENTIMENT AND REVIEW TAGS
        if (keyword.length > 10) {

            option.act = "default";
            /*GET SENTIMENT + REVIEW TAGS*/
        }

        /*GET DATA*/
        $.getJSON(url, option, function (data) {

            console.log(data);

            var text = "";

            /*GET SENTIMENT*/
            var sentiment = data.sentiment;

            //该情感搭配的极性（0表示消极，1表示中性，2表示积极）
            /*RENDER SENTIMENT*/
            text = NLP.sentiment_render(sentiment);

            /*GET REVIEW + SENTIMENT*/
            if (option.act = "default") {

                //get more review tags
                if (data.review != undefined && data.review != "") {

                    var review = data.review;

                    console.log(review);

                    text += NLP.review_render(review);


                }

            }

            /*FINAL RENDER*/
            render(text);


            return;
        })
    },

    /*RENDER*/
    review_render: function (review) {

        var text = "";

        review.forEach(function (item) {

            var review_item = item.prop + item.adj;

            //RENDER SENTIMENT EMOTION
            var sentiment_emo = NLP.sentiment_emo(item.sentiment);

            text += '<div class="chip">' + review_item +
                '<i class="material-icons" style="color: orange;">' +
                sentiment_emo.emotion + '</i>' +
                '</div>';

        })

        return text;
    },


    /*RENDER*/
    sentiment_render: function (data) {

        /*CALCULATE*/
        var positive_prob = Math.round(data.positive_prob * 100) + "%";
        var negative_prob = Math.round(data.negative_prob * 100) + "%";


        console.log(positive_prob);


        var text = ' <div class="row sentiment-row"> ' +

            /*TICH CUC*/
            '<div class="col s2" style="padding: 0px;"><i class="material-icons medium" style="color: orange; font-size: 3rem">sentiment_very_satisfied</i></div>' +

            /*SENTIMENT BAR*/
            '<div class="col s8" style="padding: 0px;">' +
            '<div class="progress" style="height: 30px;"> <div class="determinate orange center" style="width:' + positive_prob + '">' + positive_prob + '</div> ' +
            '<div class="right">' + negative_prob + '</div> ' +

            '<i class="material-icons"></i></div>' +
            '</div>' +

            /*TIEU CUC*/
            '<div class="col s2" style="padding: 0px;"><i class="material-icons medium" style="color: #039be5; font-size: 3rem;">sentiment_very_dissatisfied</i></div>' +
            '</div>';


        //RENDER SENTIMENT EMOTION
        var sentiment_emo = NLP.sentiment_emo(data.sentiment);

        text += '<div class="row">' +
            '<div class="col s12">' +
            'Thái độ: (Tích cực: ' + positive_prob + ' - Tiêu cực: ' + negative_prob + ') ' +

            '<span style="font-weight: bold; color: orange">' + sentiment_emo.text + '</span>' +
            '<i class="material-icons" style="color: orange;">' + sentiment_emo.emotion + '</i>' +

            '</div></div>';

        return text;
    },

    //RENDER EMO BY SENTIMENT
    sentiment_emo: function (sentiment) {

        /*sentiment*/
        var sentiment_text, sentiment_emo;

        switch (sentiment) {

            case 0:
                sentiment_text = "Tiêu cực";
                sentiment_emo = "sentiment_very_dissatisfied";
                break;
            case 2:
                sentiment_text = "Tích cực";
                sentiment_emo = "sentiment_very_satisfied";
                break;
            default:
                sentiment_text = "Trung lập";
                sentiment_emo = "sentiment_neutral";

                break;
        }

        var result = {

            text: sentiment_text,
            emotion: sentiment_emo
        }

        return result

    },

    /*TRANSLATOR*/
    translator: function (keyword, render) {

        var url = "./google_translate.php";
        var option = {

            content: keyword,
            devmode: devmode,
            lang: Chat.g_language,
            target: voice_conf.translate_lang
        };


        // loading("load", bubble, "append");
        $.getJSON(url, option, function (data) {

            console.log(data);

            var result = data.result;

            /*RENDER TO NEW BUBBLE*/
            render(result);

        });

        return;
    }
}
