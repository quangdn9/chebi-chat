/**
 * Created by donhatquang on 8/11/17.
 */

var Baidu = {

    /*PRIVATE*/
    voiceConf: {

        per: 0,
        spd: 4,
        speed: 1
    },

    voiceSource: "",
    myAudio: {},

    voiceCurrentContent: "", //current

    /*INIT*/
    __construct: function () {

        /*voice nhi nhanh*/
        this.voiceConf.per = 4;
        /*mp3*/
        this.myAudio = document.getElementById('mp3_audio');

        return;
    },

    /*PLAY AUDIO FROM TEXT*/
    baiduVoice: function (content, lang) {

        var source = "/textToVoice.php?title=simi&text=" + content + "&lang=" + lang;

        source += "&" + $.param(this.voiceConf);
        console.log("Baidu voice source: " + source);

        if (devmode)
            source = "aidemo.mp3";


        /*ADD SOURCE*/
        this.myAudio.src = source;

       /* setTimeout(function () {
            Baidu.AudioLoaded();

        }, 500);*/

        return;
    },
    /*LOAD EVENT*/
    AudioLoaded: function () {
        // $("#output").append("Audio load finish");
        Baidu.myAudio.play();
    },

    /*PLAY SOUND WHEN CLICK EVENT*/
    playSound: function (e) {

        var obj = $(e);
        var bubble = obj.parents(".chat_content").find(".bubble").clone();


        /*FIND BUBLE TEXT*/
        var lang = obj.attr("data-lang");

        if (bubble.find(".pinyin") != 0) {

            bubble.find(".pinyin").remove();
        }

        var content = bubble.text();

        //BEGIN
        /*current lang*/

        /*PLAY VOICE*/
        this.voiceByContent(content, lang);

        //-----------

        // *AUDIO PLAYING*!/
        // bubble.addClass("orange speaking");

        obj.addClass("speaking").css("color","orange");

        return false;
    },

    /*PLAY BY CONTENT AND LANGUAGE*/
    voiceByContent: function (content, lang) {

        var lang = lang || "vn";
        // alert(lang);

        //NORMAL PLAY WITH TEXT TO VOICE PHP
        var normalPlay = function () {

            var myAudio = this.myAudio;

            /*CHECK IF TOGGLE CURRENT AUDIO ELEMENT*/
            if (content == this.voiceCurrentContent) {

                if (myAudio.duration > 0 && !myAudio.paused) {

                    /*PAUSE*/
                    myAudio.pause();

                }
                else myAudio.play();
            }

            /*NEW AUDIO CONTENT - DIFFERENT WITH CURRENT*/
            else {

                /*ADD NEW VOICE*/
                /*play voice*/
                Baidu.baiduVoice(content, "zh");
            }
        };

        /*USE BAIDU VOICE FOR CHINESE*/
        if (lang == "zh") {

            /*VOICE FOR CHINESE (NHI NHANH)*/
            normalPlay();

        }

        /*VOICE FOR OTHER LANGUAGE*/
        else {

            this.responseVoice(content, lang);
        }
    },

    endSound: function () {

        console.log("Finish sound");
        $(".speaking").removeClass("speaking").css("color","");

        return;
    },


    //VOICE RESPONSIVE.ORG
    responseVoice: function (content, lang) {

        /*responsiveVoice.getVoices();*/
        var voice_lang = "";

        switch (lang) {

            case "zh":
                voice_lang = "Chinese Female";
                break;
            case "hk":
                voice_lang = "Chinese (Hong Kong) Female";
                break;
            case "en":
                voice_lang = "US English Female";
                break;

            case "ja":
                voice_lang = "Japanese Female";
                break;

            case "fr":
                voice_lang = "French Female";
                break;

            default:
                voice_lang = "Vietnamese Male";

        }

        /*SPEAKING*/

        //responsiveVoice define by javascript. responsiveVoice.com
        console.log("lang: "+ lang + voice_lang + " - " + content);


        /*GET VOICE LIST*/
        //var voicelist = responsiveVoice.getVoices();

        /*IS PLAYING*/
        if (responsiveVoice.isPlaying()) {

            console.log("I hope you are listening");

            /*PAUSE*/
            //responsiveVoice.pause();
            responsiveVoice.cancel();
        }
        //else {

            responsiveVoice.speak(content, voice_lang, {

                rate: this.voiceConf.speed,
                onend: this.endSound

            });
        //}

        return false;
    }
}

