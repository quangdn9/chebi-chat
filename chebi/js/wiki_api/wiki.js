/**
 * Created by donhatquang on 9/12/17.
 */
var Wiki = {

    data: "",
    detail: "",

    /*------*/

    __construct: function (data) {

        this.data = data;

        return;
    },

    /*SEARCH LIST WIKI*/
    getList: function (data) {

        // var data = this.data;

        console.log(data);


        var section = data[1];
        var detail = data[2];
        var links = data[3];

        /*collapsible-accordion popout*/
        var text = '<ul class="collapsible" data-collapsible="accordion">';

        // text += '</ul>';

        /*LIST DATA*/
        section.forEach(function (item, key) {

            var id = key + 1;
            var content = detail[key];
            var link = links[key];

            /*GET KEYWORD*/
            // link = link.split("/");
            // var keyword = link.pop();


            if (id >= 9) {

                id = "filter_9_plus";
            }
            else
                id = "filter_" + id;

            /*---------*/

            text += '<li>' +
                '<div class="collapsible-header">' +
                '<i class="material-icons">' + id + '</i>' + item + '</div>' +

                '<div class="collapsible-body"><span class="wiki_content">' + content + '</span>';

            /*VOICE BUTTON*/
            text += '<a class="volume_up sim_speak" href="#!" data-lang="'+Chat.g_language+'" onclick="Wiki.playSound(this);" > ' +
                '<i class="material-icons small volume_up_loading">volume_up</i></a>';


            /*DETAIL BUTTON*/
            text += '<div class="row wiki_detail"><div class="col s12" style="text-align:center; padding: 0px;"> ';

            // '<a onclick="Wiki.getDetail(this);" data-url="' + keyword + '" class="waves-effect waves-light btn orange">Chi tiết</a>' +

            /*FIRST BUTTON */
            if (key == 0) {
                text += '<a onclick="Wiki.getDetail(this);" data-url="' + link + '" class="waves-effect waves-light btn orange wiki_current">Chi tiết</a>';
            }

            else {
                text += '<a onclick="Wiki.getDetail(this);" data-url="' + link + '" class="waves-effect waves-light btn orange">Chi tiết</a>';
            }

            text += '</div></div>';

            /*END BUTTON*/

            text += '</div> </li>';

        });

        text += '</ul>';


        return text;
    },


    /*GET WIKI DETAIL*/
    getDetail: function (obj, type) {

        var type = type || "default";

        var lang = Chat.g_language;
        var url = "./api/wiki_detail.php";

        var fullURL = "";

        /*ADD LOADING*/
        if (type == "default") {

            obj = $(obj);

            obj.hide();
            loading("load", obj);

            fullURL = $(obj).attr("data-url"); /*FULL-URL*/

        }

        /*MODAL TYPE*/
        else {

            fullURL = obj["data-url"];
            loading("load", $(".wikiModal_content"), "prepend");

            $("#wikiModal").modal("open");
        }


        /*DETAIL*/
        var initDetail = function (data) {

            // console.log(data);
            var text = data.parse.text["*"];

            /*GET DATA*/
            Wiki.data = text;

            // console.log(text);
            // obj.parent().append(text);

            /*JQUERY OBJECT OF CONTENT*/
            var html = $(text);

            $(".vcard").css({float: "left"});



            $(".mw-parser-output, .mf-section-0").find("p").css({"text-align": "left"});

            /*BINDING EFFECT*/
            html = init_binding(html);

            /*INSERT AFTER BUTTON*/
            if (type == "default") {

                obj.after(html);

                /*-------LOADED FINISH-------*/
                /*REMOVE BUTTON*/
                obj.remove();

            }

            else if(type == "modal") {

                /*INIT TO MODAL*/
                $(".wikiModal_content").html(html);

            }

            /*FULL WIDTH INFO BOX IN MOBILE*/
            if (Device.mobile != "default") {

                $(".infobox").css({
                    width: "100%"
                });

            }

            /*LOADED FINISH*/
            loading("loaded");

            return;
        };

        /*BINDING EFFECT*/
        var init_binding = function (html) {

            /*-------REMOVE CONTENT----------*/

            /*REMOVE OTHER SECTION*/
            html.find(".mf-section-0").nextAll().remove();

            /*REMOVE IF HAVE MENU*/
            html.find("#toc").nextUntil(".noprint").remove();
            html.find("#toc, .noprint").remove();

            /*LINK EVENT FOR MORE DETAIL*/
            html.find("a").on("click", function () {

                // console.log(fullURL);
                var link = $(this).attr("data-url");
                console.log(link);

                /*GET DETAIL*/
                Wiki.getDetail({"data-url": link}, "modal");

                return false;

            }).each(function () {

                console.log(fullURL);

                /*ADD DATA-URL*/
                var link = $(this).attr("href");
                var wikiHost = fullURL.split("/")[2];

                link = "https://"+wikiHost+link;

                $(this).attr("data-url",link);

                // console.log(link);

            });

            return html;

        };

        /*GET JSON WIKI DETAIL*/
        var param = {

            lc: lang,
            keyword: fullURL,
            "devmode": devmode

        };

        $.getJSON(url, param, initDetail);

        return;
    },


    /*PLAY SOUND FOR WIKI*/
    playSound: function (obj) {

      obj = $(obj);

      var content = obj.parent().find(".wiki_content").text();
      var lang = obj.attr("data-lang");

      console.log(content);

      /*PLAY VOICE*/
      Baidu.voiceByContent(content, lang);

      return false;
    }
}