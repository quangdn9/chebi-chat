/**
 * Created by donhatquang on 9/15/17.
 */

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};


function resizeIframe(obj) {

    obj.style.height = (obj.contentWindow.document.body.scrollHeight - 30) + 'px';
    // console.log(obj.style.height);
    return;
}

/*fix number with comma*/
function numberWithCommas(x) {

    var x = x || 0
    if (x == undefined || x == "null" || x == null)
        return 0;

    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/*ADD MORE ZERO*/
function padZerosToLength(value, minLength, padChar) {

    var iValLength = value.toString().length;
    return ((new Array((minLength + 1) - iValLength).join(padChar)) + value);
}

/*FIND IN OBJECT*/
function getObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getObjects(obj[i], key, val));
        } else if (i == key && obj[key] == val) {
            objects.push(obj);
        }
    }
    return objects;
}


/*ADD LOAD*/
function loading(status, obj, style) {

    var style = style || "after";

    var obj = obj || false;

    if (status == "load") {

        var loadtext = '<div class="progress loading white"> ' +
            '<div class="indeterminate orange"></div></div>';

        if (style == "prepend") {

            obj.prepend(loadtext);
        }
        else if (style == "append") {

            obj.append(loadtext);
        }

        else {

            obj.after(loadtext);
        }

    }

    /*FINISH*/
    else {

        $(".loading").remove();
    }

    return;

}

/*SHOW INFO*/
var showInfo = function (s) {

    //console.log(s);
    Materialize.toast(s, 1000);
}

function linebreak(s) {
    return s.replace(two_line, '<p></p>').replace(one_line, '<br>');
}

var mobile_param = function () {

    if (Device.browser.indexOf("iPhone") != -1) {

        Device.mobile = "iphone"
    }
    else if (Device.browser.indexOf("Android") != -1) {

        Device.mobile = "android"
    }
    else {

        Device.mobile = "default"
    }

    //SET UP BACKGROUND
    console.log("User device is: ");
    console.log(Device);

    return;
}

/*CHANGE LANGUAGE PARAM FOR CHEBI CLIENT-SIDE*/
var changeLanguage = function (voice_lang, type) {

    var type = type || "voice";

    if (type == "translate") {

        voice_conf.translate_lang = voice_lang;

        console.log(voice_conf.translate_lang);

        return;
    }

    /*SET GOOGLE VOICE LANG*/
    voice_conf.lang = voice_lang;

    /*CHANGE CHEBI LANGUAGE*/
    Chat.g_language = langMap[voice_lang];

    // var chebi_lang = setLangMap(voice_lang,"chebi");

    console.log("Voice record language: " + voice_lang + ", Simsimi lang: " + Chat.g_language);

    return;
}



/*CONVERT LANGUAGE CHEBI & GOOGLE VOICE LANG*/
var setLangMap = function (lang, type) {

    var type = type || "g_lang";
    var result = false;

    /*AUTO DETECT LANGUAGE*/
    if (lang == "detect") {

        lang = "vn";
    }

    $.each(langMap, function (g_lang, chebi_lang) {

        if (type == "g_lang" && lang == chebi_lang) {

            result = g_lang;

        }
        else if (lang == g_lang) {

            result = chebi_lang;
        }

    })

    return result;
}


