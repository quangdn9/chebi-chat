/**
 * Created by donhatquang on 10/5/17.
 */
/*INIT FOR RESPONSE PROPERTY*/
$.extend(Chat, {


    chatApp: function (data) {

        var text = "";

        /*PICTURE PROPERTY*/
        if (data.type == "picture") {

            // console.log(data);

            var url = data.url;
            var image = data.image;

            image = "<img src='" + image + "' style='width: 200px;'/>"
            text = "<a href='" + url + "' target='_blank'>" + image + "</a>";

            return text;
        }

        /*GOOGLE MAP*/
        else if (data.type == "map") {

            var keyword = data.keyword;

            /*SEARCH*/
            Gmap.trigger(keyword);
        }

        /*NEWS*/
        else if (data.type == "list") {

            var text = this.chatList(data.list);

            /*ADD CHAT*/
            Chat.addChat("simi", text, "finish");
        }

        /*URL*/
        else if (data.type == "url") {

            var text = data.text+'<a href="'+data.url+'" target="_blank">Bấm vào đây</a>';

            /*ADD CHAT*/
            Chat.addChat("simi", text, "finish");
        }

        /*NEWS*/
        else if (data.type == "stroke") {

            var text = this.strokeList(data.list);

            /*ADD CHAT*/
            Chat.addChat("simi", text, "finish");
        }

        /*NEWS*/
        else if (data.type == "photos") {

            var text = this.photosList(data.list);
            var nextPageURL = data.nextpage;

            text += '<div class="row"><div class="col s12" style="text-align: center"> ' +
                '<a onclick="Chat.loadMore(\'' + nextPageURL + '\', this);" class="waves-effect waves-light btn loadMore"thêm>Xem thêm</a>' +
                '</div></div>';

            /*ADD CHAT*/
            Chat.addChat("simi", text, "finish");
        }


        /*YOUTUBE INIT*/
        else if (data.type == "youtube") {

            var text = Youtube.youtubeList(data.list);

            /*CHECK next PAGE*/
            if (data.nextPage) {

                var nextPageURL = data.url;

                text += '<div class="row"><div class="col s12" style="text-align: center"> ' +
                    '<a onclick="Chat.loadMore(\'' + nextPageURL + '\', this);" class="waves-effect waves-light btn loadMore"thêm>Xem thêm</a>' +
                    '</div></div>';
            }

            /*ADD CHAT*/
            Chat.addChat("simi", text, "finish");

        }

        /*WIKI LIST*/
        else if (data.type == "wiki") {

            var list = data.list;

            Wiki.__construct(list);

            var text = Wiki.getList(list);

            /*ADD CHAT*/
            Chat.addChat("simi", text, "finish");


            /*EFFECT*/
            setTimeout(function () {

                $('.collapsible:last').collapsible();

                // Open
                $('.collapsible:last').collapsible('open', 0);

                $(".collapsible .collapsible-body").css("padding", "0.5em");

                /*OPEN MORE*/
                $(".wiki_current").trigger("click").removeClass("wiki_current");

            }, 1000);
        }


        /*E-COMMERCE SUPPORT*/
             else if (data.type == "taobao") {

                 var ecommerce = [

                     {
                         name: "JD.com",
                         link: "https://search.jd.com/Search?enc=utf-8&keyword=",
                         lang: "zh-CN"
                     },

                     {
                         name: "Taobao",
                         link: "https://world.taobao.com/search/search.htm?_input_charset=utf-8&q=",
                         lang: "zh-CN"
                     },

                     {
                         name: "1688",
                         link: "https://search.jd.com/Search?enc=utf-8&keyword=",
                         lang: "zh-CN"
                     },

                     {
                         name: "Amazon",
                         link: "https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=",
                         lang: "en"
                     },

                     {
                         name: "Lazada",
                         link: "http://www.lazada.vn/catalog/?q=",
                         lang: "default"
                     },

                 ]

                 var text = '<div class="collection ecommerce">';

                 /*LIST E-COMMERCE*/
                 for (i in ecommerce) {

                     var item = ecommerce[i];
                     var keyword = "";

                     /*GET TRANSLATE*/
                     if (item.lang == "default") {

                         keyword = data.keyword;
                     }

                     else {

                         /*FIND OBJECT*/
                         var translate = getObjects(data.translate, "lang", item.lang);

                         console.log("E-COMMERCE"+item.lang);

                         if (translate.length != 0)
                             keyword = translate[0].result;

                     }

                     /*CHECK KWD*/
                     if (keyword != "") {

                         var url = item.link + keyword;
                         text += '<a href="' + url + '" target="_blank" class="collection-item">Mua ' + data.keyword + ' trên ' + item.name + '</a>';
                     }

                 }

                 text += '</div>';

                 /*ADD CHAT*/
                 Chat.addChat("simi", text, "finish");
             }

        return false;
    },

    /*next page*/
    loadMore: function (url, obj) {

        var obj = $(obj);

        $.getJSON("chebi_chat.php?"+url, function (data) {

            Chat.chatApp(data.property);

            /*REMOVE BUTTON*/
            obj.remove();
        });

        return;
    },

    /*DETECT CHINESE*/
    langDetect: function (keyword, lang) {

        var lang = lang || "chinese";

        /*CHECK CHINESE*/
        var re = /[^\u4e00-\u9fa5]/;

        if (re.test(keyword)) {

            return false;
        }

        return true;

    },

    /*translate*/
    addPropertyChat: function (act, obj) {

        // var target = target || "chat_simi";

        var obj = $(obj).parents(".chat_content");

        /*CLONE TO NEW OBJECT*/
        var newObject = obj.clone();
        var bubble = newObject.find(".bubble ruby");

        /*REMOVE PINYIN*/
        bubble.find("rt").remove();

        /*FIND BUBLE TEXT*/
        var keyword = bubble.text();
        console.log(keyword);

        /*LOADING*/
        var obj_bubble = $(obj).find(".bubble");

        /*LOADING*/
        loading("load", obj_bubble, "append");

        //RENDER FUNCTION
        var render = function (text) {

            /*add content*/
            bubble.html(text);

            /*REMOVE*/
            newObject.find(".g_translate, .sentiment").remove();
            newObject.addClass("chat_translate");

            /*ADD VOICE */
            var speaker = newObject.find(".sim_speak");

            // var current_lang = speaker.attr("data-lang");

            /*SPEAK VOICE*/

            /*SETTING FOR DATA-LANG */
            var detectlang = setLangMap(voice_conf.translate_lang);
            speaker.attr("data-lang", detectlang);

            console.log(detectlang);


            /*fix button style*/
            newObject.find(".chebi-action-simi ul").css("left", "-350px");
            newObject.find(".chebi-action-user ul").css("right", "0px");

            /*GET SENTIMENT*/
            if (act == "sentiment") {

                // *REMOVE BUTTON CONTROL*/
                newObject.find(".chebi-action-btn").remove();
            }

            /*ADD TRANSLATE BEFORE*/
            newObject.insertAfter(obj);


            /*FINISH LOAD*/
            loading("finish");

            return;

        };


        /*TRANSLATE*/
        if (act == "translator") {

            /*DISABLE BUTON*/
            obj.find(".g_translate").addClass("disabled");

            /*TRANSLATE */
            NLP.translator(keyword, render);
        }

        /*CHECK SENTIMENT*/
        else if (act == "sentiment") {

            /*DISABLE BUTON*/
            obj.find(".sentiment").addClass("disabled");

            bubble.css("width", "300px")

            /*GET SENTIMENT DATA*/
            NLP.sentiment(keyword, render);


        }

        return;
    },

    /*PICTURE LIST*/
   photosList: function(data) {

        var text = '<ul class="collection">';

        for (count in data) {

            var item = data[count];

            /*AVATAR*/
            text += '<li class="collection-item avatar">' +
                '<img src="' + item["thumbnail"] + '" alt="" class="circle large" style="width: 60px; height: 60px; left: 5px;">';

            /*TITLE*/
            text += '&nbsp;&nbsp; <a href="' + item["thumb_max"] + '" target="_blank"><span class="title">' + item["title"] + '</span></a>';

            /*SOURCE*/
            text += "<p>" + item["description"] + "</p>";

        }

        text += "</ul>";

        return text
    },

    /*STROKE LIST*/
    strokeList: function (data) {

        var text = "<div class='stroke'>";

        for (count in data) {

            var item = data[count];
            text += "<img src='"+item+"'/>";
        }

        text += "</div>";

        return text;
    },

    /*ADD CHAT LIST PROPERTY*/
    chatList: function (data) {

        var text = '<ul class="collection">';

        for (count in data) {

            var item = data[count];

            /*AVATAR*/
            text += '<li class="collection-item avatar">' +
                '<img src="' + item["icon"] + '" alt="" class="circle large" style="width: 60px; height: 60px; left: 5px;">';

            /*TITLE*/
            text += '&nbsp;&nbsp; <a href="' + item["detailurl"] + '" target="_blank"><span class="title">' + item["name"] + '</span></a>';

            /*SOURCE*/
            text += "<p>" + item["info"] + "</p>";

        }

        text += "</ul>";

        return text

    }

});