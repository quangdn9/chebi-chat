/**
 * Created by donhatquang on 8/11/17.
 */

var voice_conf = {

    lang: "vi-VN", //INPUT RECORD LANG,
    translate_lang: "zh"

};

var langMap = {

    "cmn-Hans-CN": "zh",
    "yue-Hant-HK": "hk", /*cmn-Hans-HK*/
    "en-US": "en",
    "vi-VN": "vn",
    "ja-JP": "ja",
    "fr-FR": "fr"
};

var Chat_config = {

    botname: "Chebi",
    hello: {
        default: "Chebi rất vui được nói chuyện cùng bạn",
        chinese: "Chebi 很高兴可以陪你聊天",
        japanese: "Chebi あなたに会えてよかった"
    },

    background: "",
    background_mode: "chebi",
    theme_color: "orange"
    // language: "vn"
}

/*CHECK DEVICE TYPE*/
var Device = {

    browser: navigator.userAgent,
    mobile: ""
}

/*GOOGLE API*/

/*START-UP APP*/
function startup() {

    /*MAP*/
    // Gmap.initMapLocate();

    //CHECK KEYWORD PARAM
    if (keyword) {

        $("#reqText").val(keyword);

        setTimeout(function () {

            Chat.send();

        }, 1000);
    }


    // if (Chat.g_language == "vn") {

    /*SETUP GOOGLE MAP*/
    Gmap.initMapLocate(function(){});



    /*START-UP*/

    /*SET PAGE THEME*/
    if (page != "") {

        Pages.getPageData(page);
    }

    return;

}

/*PAGE LOAD READY*/
$(function () {

    loading("load", $("#chat_body"));

    /*BIND ACT*/
    Bindact();

    //SET UP MOBILE
    mobile_param();

    /*VOICE INIT*/
    Record.__construct();

    /*BAIDU AUDIO*/
    Baidu.__construct();

    /*USER*/
    Chebi_user.__construct();


    /*STARTUP*/
    setTimeout(function () {

        startup();
    },2000);

    /*FIX FOR DEBUGGER*/
    if (devmode) {

        console.log("--DEV MODE BEGINNING --");
    }

    /*CHECK LOGINLOGIN*/
    Firebase.checkStatus();

})