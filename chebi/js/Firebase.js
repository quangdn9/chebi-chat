/**
 * Created by donhatquang on 9/5/17.
 */
var Logined = false;
const CLIENT_ID = "897401007778-3pj8s0cg90k07gpb7mg4rul3pehl07d4.apps.googleusercontent.com";
Firebase = {

    /*PRIVATE*/
    provider: {},
    user: "",
    /**
     * @return {string} The email signInMethod from the configuration.
     */
    getEmailSignInMethod: function () {
        var config = this.parseQueryString(location.hash);
        return config['emailSignInMethod'] === 'password' ?
            'password' : 'emailLink';
    },


    /**
     * @param {string} queryString The full query string.
     * @return {!Object<string, string>} The parsed query parameters.
     */
    parseQueryString: function (queryString) {
        // Remove first character if it is ? or #.
        if (queryString.length &&
            (queryString.charAt(0) == '#' || queryString.charAt(0) == '?')) {
            queryString = queryString.substring(1);
        }
        var config = {};
        var pairs = queryString.split('&');
        for (var i = 0; i < pairs.length; i++) {
            var pair = pairs[i].split('=');
            if (pair.length == 2) {
                config[pair[0]] = pair[1];
            }
        }
        return config;
    },


    getUiConfig: function () {
        return {
            'callbacks': {
                // Called when the user has been successfully signed in.
                'signInSuccessWithAuthResult': function (authResult, redirectUrl) {
                    if (authResult.user) {
                        handleSignedInUser(authResult.user);
                    }
                    if (authResult.additionalUserInfo) {
                        document.getElementById('is-new-user').textContent =
                            authResult.additionalUserInfo.isNewUser ?
                                'New User' : 'Existing User';
                    }
                    // Do not redirect.
                    return false;
                }
            },
            // Opens IDP Providers sign-in flow in a popup.
            // 'signInFlow': 'popup',
            'signInOptions': [
                // TODO(developer): Remove the providers you don't need for your app.
                {
                    provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                    // Required to enable this provider in One-Tap Sign-up.
                    authMethod: 'https://accounts.google.com',
                    // Required to enable ID token credentials for this provider.
                    clientId: CLIENT_ID
                },
                {
                    provider: firebase.auth.FacebookAuthProvider.PROVIDER_ID,
                    scopes: [
                        'public_profile',
                        'email',
                        'user_likes',
                        'user_friends'
                    ]
                },
                /*     firebase.auth.TwitterAuthProvider.PROVIDER_ID,
                     firebase.auth.GithubAuthProvider.PROVIDER_ID,
                     {
                         provider: firebase.auth.EmailAuthProvider.PROVIDER_ID,
                         // Whether the display name should be displayed in Sign Up page.
                         requireDisplayName: true,
                         signInMethod: this.getEmailSignInMethod()
                     },
                     {
                         provider: firebase.auth.PhoneAuthProvider.PROVIDER_ID,
                         /!*   recaptchaParameters: {
                                size: getRecaptchaMode()
                            }*!/
                     },
                     {
                         provider: 'microsoft.com',
                         providerName: 'Microsoft',
                         buttonColor: '#2F2F2F',
                         iconUrl: 'https://docs.microsoft.com/en-us/azure/active-directory/develop/media/howto-add-branding-in-azure-ad-apps/ms-symbollockup_mssymbol_19.png',
                         loginHintKey: 'login_hint'
                     },*/
                firebaseui.auth.AnonymousAuthProvider.PROVIDER_ID
            ],
            // Terms of service url.
            'tosUrl': 'https://www.google.com',
            // Privacy policy url.
            'privacyPolicyUrl': 'https://www.google.com',
            'credentialHelper': CLIENT_ID && CLIENT_ID != "897401007778-3pj8s0cg90k07gpb7mg4rul3pehl07d4.apps.googleusercontent.com"?
                firebaseui.auth.CredentialHelper.GOOGLE_YOLO :
                firebaseui.auth.CredentialHelper.ACCOUNT_CHOOSER_COM
        };
    },


    /*FIREBASE LOGIN*/
    __loginInit: function () {

        // FirebaseUI config.


        var uiConfig = {
            signInSuccessUrl: 'https://chebichat.com',

            'signInOptions': [
                // Leave the lines as is for the providers you want to offer your users.


                 firebase.auth.GoogleAuthProvider

                // firebase.auth.FacebookAuthProvider.PROVIDER_ID,
            ],
            'callbacks': {
                'signInSuccess': function (currentUser, credential, redirectUrl) {
                    // Do something.
                    // Return type determines whether we continue the redirect automatically
                    // or whether we leave that to developer to handle.
                    if (currentUser.uid != undefined && currentUser.uid != null) {

                        //로그인 (최초 로그인시 서버에서 가입처리함)
                        //Firebase.userLogined(currentUser, credential);

                        console.log(currentUser);

                    } else {

                        showInfo("Đăng nhập thất bại");
                        // window.close();
                    }
                }
            }
        };

        // Initialize the FirebaseUI Widget using Firebase.
        var ui = new firebaseui.auth.AuthUI(firebase.auth());

        // The start method will wait until the DOM is loaded.
        ui.start('#firebaseui-auth-container', Firebase.getUiConfig());


        this.openLogin();

        return;
    },

    openLogin: function () {

        $("#login_modal").modal("open");

        return;
    },

    /*USER INFO*/
    userLogined: function (user, credential) {

        // This gives you a Facebook Access Token. You can use it to access the Facebook API.
        var token = "";

        //GET USER
        console.log("GET ALL USER INFO");
        // console.log(user);

        if (credential != undefined)
            token = credential.accessToken;

        // User is signed in.
        var displayName = user.displayName;
        var email = user.email;
        var emailVerified = user.emailVerified;
        var photoURL = user.photoURL;
        var uid = user.uid;
        var phoneNumber = user.phoneNumber;
        var providerData = user.providerData;


        /*GET TOKEN*/
        user.getIdToken().then(function (accessToken) {

            console.log('Signed in');

            if (token == "") token = accessToken;

            // document.getElementById('sign-in').textContent = 'Sign out';
            var userinfo = {

                displayName: displayName,
                email: email,
                emailVerified: emailVerified,
                phoneNumber: phoneNumber,
                photoURL: photoURL,
                uid: uid,
                accessToken: token,
                providerData: providerData
            };


            console.log("HAVE USER " + displayName);

            /*USER INFO GLOBAL*/
            Chebi_user.userinfo = userinfo;

            console.log(Chebi_user.userinfo);

            /*GET INFO FROM DB*/
            Firebase.getUserData(uid);

            // Firebase.writeUserData(userinfo);
            // console.log(userinfo);

        });

        return;

    },

    /*LOGOUT FROM FIREBASE*/
    logout: function () {

        firebase.auth().signOut().then(function () {

            showInfo('Signed Out');

            //$("#anonymous_btn_wrap").show();
            //$("#user_btn_wrap").hide();

            window.location.reload();

        }, function (error) {
            showInfo('Sign Out Error', error);
        });

        return;

    },

    /*CHECK USER LOGIN OR NOT*/
    checkStatus: function () {

        firebase.auth().onAuthStateChanged(function (user) {

            /*HAVE USER*/
            if (user) {

                // Firebase.user = user;
                Firebase.userLogined(user);

                /*SET STATUS*/
                Logined = true;

                /*close modal*/
                $("#login_modal").modal("close");

                // User is signed out.
            } else {

                console.log('Signed out');

                /*OPEN LOGIN WINDOW*/
                Firebase.__loginInit();

                /*PERSONALIZE*/
                Chebi_user.personalize();

                //SAY HELLO to anonymous
                if (keyword == '') {

                    Chebi_user.welcome();
                }
            }

            console.log("LOGIN STATUS: " + Logined);


            /*FINISH LOAD*/
            loading("finish");

        }, function (error) {

            console.log(error);
        });

        return;
    },

    /*GET USER INFO*/
    getUserData: function (uid) {

        var uid = uid || firebase.auth().currentUser.uid;

        /**/
        console.log("Get userinfo data " + uid);

        $.get("api/get_user.php", {

            uid: uid,
            devmode: devmode

        }, function (result) {

            console.log(result);

            if (result == null) {

                console.log("THE FIRST TIME LOGIN");
                Firebase.writeUserData();

                /*INIT WHEN USER LOGIN*/
                Chebi_user.userInit(true);
            }

            /*CHECK IF THE FIRST TIME CONNECT TO DB, CREATE USERINFO TO DB BY UID*/

            else {

                /*-----------*/

                var userinfo = result;


                // data = userinfo;
                Chebi_user.userinfo = userinfo;

                /*SET PROPERTY FROM DATABASE*/
                Chebi_user.property = userinfo.property;

                /*SET USER INIT FROM CONFIG*/
                // Chat_config = userinfo.config;

                showInfo("CONNECT SUCCESSFUL");

                /*INIT WHEN USER LOGIN*/
                Chebi_user.userInit(true);
                /*---------*/
            }


        });

        /*firebase.database().ref('/users/' + uid).once('value').then(function (snapshot) {

         // return;
         // result = "CONNECT SUCCESSFUL";


         console.log("Snapshot from Database:");
         console.log(snapshot.val());

         if (snapshot.val() == null) {

         return;
         }

         });*/

        return;
    },

    /*WRITE DATA*/
    writeUserData: function () {

        /*SET PROPERTY*/
        var userinfo = Chebi_user.userinfo;

        var property = Chebi_user.setProperty();

        /*ADD PROFILE*/
        userinfo.property = property;

        console.log(userinfo);

        /*WRITE DATA TO DB*/
        var uid = userinfo.uid;
        var url = "api/save_user.php?devmode=" + devmode;

        // firebase.database().ref('users/' + userId).set(userinfo);

        $.post(url, {

            uid: uid,
            data: JSON.stringify(userinfo)

        }, function (result) {

            console.log(result);

            if (result == "saved") {

                showInfo("Ghi dữ liệu thành công!");
            }

        })

        return;
    }


}