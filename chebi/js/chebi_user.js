/**
 * Created by donhatquang on 9/15/17.
 */
var Chebi_user = {

    /*private*/
    userinfo: false,
    property: false, /*PARAM*/

    bg_host: "",

    __construct: function () {

        var host = "https://thuvien.tiengtrung.co/galleries/chebi_img";

        this.bg_host = host;

        return;
    },

    /*CHANGE USER INIT*/
    userInit: function (logined) {

        var logined = logined || false;

        var userinfo = this.userinfo;

        if (this.userinfo == false) {

            showInfo("Lấy thông tin bị lỗi");

        }

        //LOGOUT BUTTON
        $("#user_btn_wrap").show();
        $("#anonymous_btn_wrap").hide();


        /*SET AVATAR FOR USER*/
        $(".user_avatar").attr("src", userinfo.photoURL).show();


        /*CHECK PROPERTY*/
        if (this.property) {

            /*SET SELECT LANGUAGE*/
            var lang = this.property.language;
            this.setSelected("#lang_select", $("#lang_" + lang));

            /*SET SELECT THEME*/
            var bg_mode = this.property.background_mode;
            var option = $('#theme_select option[value="' + bg_mode + '"]');

            this.setSelected("#theme_select", option);
        }

        /*------------*/

        /*PERSON*/
        this.personalize();

        /*SAY HELLO*/
        if (keyword == '') {

            this.welcome();
        }

        return;
    },

    /*Personal*/
    personalize: function () {

        /*SET PARAM*/
        this.param_contruct();

        /*BACKGROUND SETTING*/
        this.setTheme();

        return;
    },

    /*USER CONFIG*/
    param_contruct: function () {


        /*SET RECORD VOICE BY SELECT OPTION*/
        if (page == "") {

            voice_conf.lang = $("#lang_select").val();
            Chat.g_language = langMap[voice_conf.lang];
        }

        /*--------------*/

        /*SET BACKGROUND MODE*/
        Chat_config.background_mode = $("#theme_select").val();


        if (Chat_config.background_mode == "chebi") {

            Chat_config.theme_color = "pink lighten-2";
        }


        return;
    },

    // console.log(avatar);
    /*SAY WELCOME TO USER FROM BOT*/
    welcome: function () {


        /*CHECK PAGE*/
        if (page != "") {

            // Pages.welcomePage();

            return;
        }

        /*----------*/

        var hello = "";

        if (Chat.g_language == "zh") {
            hello = "你好，";
        }

        else if (Chat.g_language == "ja") {
            hello = "あなたに会えてよかった, "
        }

        else if (Chat.g_language == "en") {
            hello = "Hello, how are you, "
        }

        else {

            hello = "Xin chào, "
        }

        /*user logined*/
        if (Chebi_user.userinfo) {

            hello += Chebi_user.userinfo.displayName;
        }

        /*NO LOGIN, WELCOME TO ANONYMOUS*/
        else {

            if (Chat.g_language == "zh" || Chat.g_language == "hk") {

                hello += Chat_config.hello.chinese;
            }
            else if (Chat.g_language == "ja") {

                hello += Chat_config.hello.japanese;
            }
            else {
                hello += Chat_config.hello.default;
            }
        }

        /*INIT*/
        Chat.addChat("simi", hello);

        return;
    },


    /*SET BACKGROUND FOR CHAT BODY*/
    setTheme: function () {

        var background = "";

        var mode = Chat_config.background_mode;

        /*CHECK PAGE*/
        if (page != "") {

            // Pages.setPageTheme();
            return;
        }

        /*---------------*/

        /*RANDOM*/
        /*if (mode == "female") {

         /!*SET BG RANDOM*!/
         var file = 1 + Math.floor(Math.random() * 1000);
         file = padZerosToLength(file, 3, 0);

         file = file + ".jpg";

         background = host + "/" + file;

         /!*GLOBAL*!/
         Chat_config.background = file;


         else if (mode == "personal" && Logined) {

         background = host + "/" + this.property.background;
         }*/

        else if (mode == "chebi") {

            // background = this.property.background;
            // background = "/chebi/image/background.png";
            background = "/assets/chebichat_design/Mascot design - background.png";

        }

        console.log("Bg: " + background);
        /*BACKGROUND - PROFILE*/


        /*--------SET CSS---------*/
        var css = {

            "background-image": "url('" + background + "')"
        };


        $("body").css(css);

        return;
    },


    /*set Property*/
    setProperty: function () {

        var param = {

            language: Chat.g_language,
            background: Chat_config.background
        };

        param = $.extend(param, Chat_config)

        this.property = param;
        console.log(this.property);

        return param;
    },

    /*SET SELECTOR*/
    setSelected: function (select, option) {

        /*REMOVE SELECT BEFORE*/
        $(select).find('option').removeAttr("selected");

        /*SET CURRENT SELECT*/
        option.prop("selected", true);

        $(select).material_select();
    }

}
