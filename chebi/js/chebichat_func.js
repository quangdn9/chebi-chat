/**
 * Created by donhatquang on 8/8/17.
 */
/*UTILITY*/
function onEnterKeyDown() {
    //enter 키인 경우만

    if (event.keyCode == 13) {

        Chat.send();
        return;
    }
}


/*CHAT CLASS FOR SIMSIMI*/
var Chat = {

    g_language: "vn",
    g_filter: 0,
    g_normalProb: 0,

    /*ABSTRACT FUNCTION*/
    /*-----------*/


    /*ADD CHAT CONTENT*/
    addChat: function (who, content, property) {

        var property = property || false;

        // CLONE CHAT BUBBLES
        $(".chat_" + who + ".dummy").clone()
            .addClass("working chat_content").insertBefore("#bottom_margin");


        /*CHAT CONTENT*/
        var chat_content_init = function () {

            /*-------*/
            /*FIX CONTENT WITH USER*/

            //BEGIN INIT, ADD HTML TO CONTENT
            $(".working .bubble").html(content);


            /*ADD SHARE BUTTON*/
            if (property == "finish") {

                Chat.addShare();
            }

            $(".working").removeClass("working dummy");

            /*ADD AVATAR CLICK*/

            /*ADD NEXT CHAT*/
            /*CHECK IF CONTENT IS IMAGE OR Something else*/
            if (property != "finish") {

                if (property) {

                    // /!*Continue add image*!/
                    Chat.addChat("simi", property, "finish");
                }

                // HAVE NOT PROPERTY -> FALSE*!/
                else {

                    // /!*AUTO PLAY WITH ZH Chinese in mobile or PC*!/
                    if (Device.mobile == "default" || Chat.g_language == "zh") {

                        $(".sim_speak:last").trigger("click");
                    }

                }
            }

            /*SCROLL WINDOW TO NEW ROW*/
            $(document).scrollTop($(document).height());

            return;
        };

        /*ADD CONTROL BUTTON*/
        Chat.addChatBtn(property, who);


        /*IF SENDER IS USER*/
        if (who == 'user') {

            $(".working .bubble").html(content);

            $(".working").removeClass("working dummy");

            $(document).scrollTop($(document).height());
        }


        /*IF SENDER IS BOT*/
        else if (who == 'simi') {

            /*display simsimi chat content*/
            setTimeout(chat_content_init, 500);
        }

    }


}
