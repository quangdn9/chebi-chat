/**
 * Created by donhatquang on 11/23/17.
 */
var Pages = {

    data: {},

    /*GET USER INFO*/
    getPageData: function (pageid) {

        $.getJSON("api/get_page.php", {

            pageid: pageid,
            devmode: devmode

        }, function (result) {

            /*INIT WHEN USER LOGIN*/
            Pages.data = result;

            console.log(result);

            // Chebi_user.userInit(true);
            Pages.setPageTheme(result);

            /*HELLO*/
            Pages.welcomePage();

            /*SET LANGUAGE*/
            Pages.setPageLanguage();

        });

        return;
    },

    /*PAGE THEME*/
    setPageTheme: function (data) {

        // console.log("set page theme");
        // console.log(data);


        var background = data.property.background;
        var logo = data.property.logo;

        var css = {

            "background-image": "url('" + background + "')"
        };

        console.log(css);


        $("body").css(css);

        var logo_url = logo.url;
        var logo_size = logo.size;

        $("#chebi-logo").attr("src", logo_url).css("height", "50px");




        return;

    },

    setPageLanguage: function() {

        /*SET PAGE LANGUAGE*/
    },

    welcomePage: function () {

        var hello = Pages.data.property.hello;

        hello = hello.default;

        /*if (Chat.g_language == "zh" || Chat.g_language == "hk") {

            hello = "欢迎你访问";
        }
        else if (Chat.g_language == "fr") {

            hello = "bonjour";
        }
        else {
            hello = hello.default;
        }*/

        /*ADD NAME*/
        // hello += " " + title;

        Chat.addChat("simi", hello);

        return false;
    },
}