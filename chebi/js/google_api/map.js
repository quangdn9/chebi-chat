var Gmap = {

    // apikey: "AIzaSyB_WEPzrCLjKaY9HVHPTdyxLFP3RbnO2Uc",
    map: false, /*have not settup*/

    markers: [],
    markers_current: [],

    //INFO WINDOW FOR MAP (WHEN CLICK)
    infowindow: "",

    searchBox: {},
    place: {},
    location: "",
    currentLocate: {},

    /*SEARCH PLAGE*/
    nearbySearch: function (place) {
        // var pyrmont = {lat: -33.866, lng: 151.196};
        // this.setupInit();


        var place = place || ["cafe", "movie_theater"];

        var map = this.map;
        var center = this.location;

        var service = new google.maps.places.PlacesService(map);
        console.log(center);

        service.nearbySearch({
            location: center,
            radius: 3000,
            keyword: place

        }, this.processResults);

        /*SET MARKER CENTER*/
        var marker = new google.maps.Marker({

            map: map,
            // icon: image,
            position: center,
            title: 'Hello World!'
        })
        this.markers_current.push(marker);

        return;
    },

    //PRINT RESULT
    processResults: function (results, status, pagination) {

        if (status !== google.maps.places.PlacesServiceStatus.OK) {

            console.log("error");
            console.log(google.maps.places.PlacesServiceStatus);
            return;

        } else {


            Gmap.place.pagination = pagination;
            Gmap.place.result = results;

            console.log(Gmap.place);
            mapInit.render(results);

            if (results.length == 0) {

                console.log("no have places");
            }

            return;
        }
    },

    /*CENTER MAP*/
    centerMap: function (zoom) {

        var zoom = zoom || 13;

        setTimeout(function () {

            /*GET LAST POSITION*/
            var last_marker = Gmap.markers_current[Gmap.markers_current.length - 1];
            var position = last_marker.getPosition();

            /*SET CENTER*/
            Gmap.map.setCenter(position);
            Gmap.map.setZoom(zoom);

        }, 200);


        console.log("center map");
    },

    /*CLICK EVENT*/
    events: function (act) {

        if (act == "fullscreen") {

            $(".gm-fullscreen-control").trigger("click");

            this.centerMap(13);

        }

        return false;
    },

    /*MARKER FOR PLACE ON MAP*/

    clearMarker: function () {

        Gmap.markers.forEach(function (marker) {

            marker.setMap(null);
        })

        return;
    },

    /*LOCATE*/
    initMapLocate: function (callback) {

        var callback = callback || false;

        /*ERROR*/
        var initMapWithError = function () {

            var pos = {
                lat: 21.0171466, lng: 105.7840758
            };

            Gmap.setupInit(pos);
        };

        /*SETUP INFO WINDOW*/

        try {
            this.infowindow = new google.maps.InfoWindow();

        } catch (e) {
            console.log(e);
        }


        // Try HTML5 geolocation.
        if (navigator.geolocation) {

            /*GET CURRENT LOCATION*/
            navigator.geolocation.getCurrentPosition(function (position) {

                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                /*CURRENT LOCATE NEVER CHANGE IN THE SESION*/
                Gmap.currentLocate = pos;

                /*init the map*/
                Gmap.setupInit(pos);

            }, function () {

                initMapWithError();

                Gmap.handleLocationError(true);
            });

        }

        else {

            initMapWithError();

            // Browser doesn't support Geolocation
            Gmap.handleLocationError(false);
        }

        /*callback after setup finish*/
        callback();


        return;
    },


    /*SETUP MAP*/
    setupInit: function (pos) {

        // alert(pos.lat + "," + pos.lng);

        var pos = pos || Gmap.location;

        /*PRINT LOCATION*/
        console.log("SET UP GOOGLE MAP");
        console.log(pos);

        /*SETTING MAP*/
        var map = new google.maps.Map(document.getElementById('map'), {
            center: pos,
            zoom: 17,
            mapTypeId: 'roadmap',
            scrollwheel: true
        });

        /*SETTING MARKER FOR CENTER*/
        var marker = new google.maps.Marker({
            map: map,
            title: "Current",
            position: pos
        });

        /*PUT CURRENT MARKER TO ARRAY*/
        this.markers_current.push(marker);

        // Gmap.searchBox = searchBox;
        Gmap.map = map;
        Gmap.location = pos;


        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {

            // mapInit.searchBox.setBounds(map.getBounds());
            var location = map.getCenter();
            var pos = {
                lat: location.lat(),
                lng: location.lng()
            };

            /*DO NOTHING*/

            //Gmap.location = pos;

            return;
        });


        /*BEGIN INIT*/
        mapInit.renderInputBox();

        //$("#mapModal").modal("open");
        $("#map").css({
            "width": "1px",
            "height": "1px"
        });

        return;
    }
    ,

    /*most important - trigger the result search*/
    trigger: function (place) {

        /*MAP*/
        $("#pac-input").val(place);

        // var input = document.getElementById('pac-input');
        // var input = $("#pac-input").val();

        /* google.maps.event.trigger(input, 'focus')
         google.maps.event.trigger(input, 'keydown', {

         keyCode: 13
         });*/

        this.nearbySearch(place);

        return;
    }
    ,

    /*HANDLE ERROR FUNC*/
    handleLocationError: function (browserHasGeolocation) {

        Materialize.toast(browserHasGeolocation ?
            'Error: The Geolocation service failed.' :
            'Error: Your browser doesn\'t support geolocation.', 4000);
        return;
    }
}








