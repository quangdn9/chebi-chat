/*PRIVATE*/
var final_transcript = '';
var ignore_onend;
var start_timestamp;

var final_words = "";
var final_words_all = new Array();

/*MAIN */
var recognizing = false;
var recognition;

var two_line = /\n\n/g;
var one_line = /\n/g;

var Record = {

    __construct: function() {

        /*NO SUPPORT VOICE INPUT*/
        if (!('webkitSpeechRecognition' in window)) {

            this.upgrade();
        }
        else {
            this.setupSpeech();
        }
    },

    /*SETUP SPEECH */
    setupSpeech: function () {

    recognition = new webkitSpeechRecognition();

    /*setup lang*/

    recognition.continuous = true;
    recognition.interimResults = true;

    recognition.onstart = function () {

        recognizing = true;

        // showInfo('info_speak_now');
        //start_img.src = '/intl/en/chrome/assets/common/images/content/mic-animate.gif';
    };

    recognition.onerror = function (event) {
        if (event.error == 'no-speech') {
            //start_img.src = '/intl/en/chrome/assets/common/images/content/mic.gif';
            showInfo('info_no_speech');
            ignore_onend = true;


        }
        if (event.error == 'audio-capture') {
            //start_img.src = '/intl/en/chrome/assets/common/images/content/mic.gif';
            showInfo('info_no_microphone');
            ignore_onend = true;
        }
        if (event.error == 'not-allowed') {
            if (event.timeStamp - start_timestamp < 100) {
                showInfo('info_blocked');
            } else {
                showInfo('info_denied');
            }
            ignore_onend = true;
        }
    };

    /*ON END CALL BACK*/
    recognition.onend = function () {

        /*CHANGE STATUS OF RECOGNIZE*/
        recognizing = false;

        if (ignore_onend) {
            return;
        }

        $("#record").removeClass(Chat_config.theme_color).find("i").text("mic_off");

        /*USER DIDN'T INPUT --> FINAL SCRIPT NO DATA*/
        if (!final_transcript) {

            // showInfo('info_start on end');

            /*START RECORD*/
            //
            // $("#record").trigger("click"); //WARNING: SOMETIME UN-LIMIT REPEAT ON MOBILE

            return;
        }

        /*FINISH THE RECORD - SEND MESSAGE*/
        /*FIX FOR EACH LANGUAGE*/
        if (voice_conf.lang == "vi-VN" && final_words_all.length > 1) {

            final_words = final_words_all.pop();
            /*get the last element*/
            var next_of_last = final_words_all.pop();

            final_words = final_words.replace(next_of_last, "");

            /*fix when the last final words duplicate contain the first FINAL words*/


        } else {

            final_words = final_words_all[0];
        }


        $("#reqText").val(final_words);


        /*SEND CHAT TO SIMSIMI*/
        Chat.send();

        showInfo('FINISH RECORD!');

        return;
    };


    /*PRINT RESULT*/
    recognition.onresult = function (event) {

        var interim_transcript = '';

        if (typeof(event.results) == 'undefined') {

            console.log("no result - undefined");

            recognition.onend = null;
            // stopRecording("underfine");

            return false;
        }


        for (var i = event.resultIndex; i < event.results.length; ++i) {

            /*FINAL TEXT*/
            if (event.results[i].isFinal) {

                final_transcript += event.results[i][0].transcript;

                /*ASSIGN RESULT TO FINAL WORDS*/
                final_words = linebreak(final_transcript);

                /*SEND TEXT TO SIMSIMI CHAT BOT*/
                //$("#reqText").val(final_words);

                console.log("Result-->final_transcript: " + linebreak(final_transcript));
                // console.log("<strong style='color: orange;'>Result-->FINAL WORD: </strong>" + final_words);


                /*STOP*/
                Record.stopRecording("final result");
                /*add text to interim word*/

            } else {
                interim_transcript += event.results[i][0].transcript;

            }
        }

        /*CAPITAL TEXT*/
        /*final_transcript = capitalize(final_transcript);*/

        /*ADD INTERIM TO FINAL WORDS IF NOT NULL*/
        if (interim_transcript != "") {

            //final_words = linebreak(interim_transcript);
            $("#reqText").val(interim_transcript);

        }

        /*WHAT HAPPEN??*/
        /*if (final_transcript || interim_transcript) {
         //showButtons('inline-block');
         }*/
    };

    return;
},

    /*START RECORD*/
    startRecording: function (event) {

        if (!('webkitSpeechRecognition' in window)) {

            upgrade();
            return;
        }

        //---------

        /*STOP RECORD*/
        if (recognizing) {

            Record.stopRecording();

            return false;
        }

        else {

            /*RESET DATA*/
            final_transcript = '';
            final_words_all = new Array();

            $("#output").html("");

            /*SETUP LANG AT EACH TIME RECORD*/
            recognition.lang = voice_conf.lang;
            console.log(voice_conf.lang);

            /*START RECORDING*/
            recognition.start();

            $("#record").addClass(Chat_config.theme_color).find("i").text("mic");


            ignore_onend = false;
            start_timestamp = event.timeStamp;
        }
        return;
    },

    /*STOP RECORD METHOD*/
    stopRecording: function (whocall) {

        whocall = whocall || "default stop";

        /*STOP RECOGNIZE*/
        recognition.stop();
        //Chat.send();

        /*SEND CHAT RESULT*/
        if (final_words != "") {

            /*FIX MULTI RECORD TIMES ON MOBILE (USE ARRAY TO FIX -> GET THE FIRST ELEMENT OF RECORD)*/
            final_words_all.push(final_words);
        }

        console.log(whocall + " --> call stop recording");
        console.log("--------------\n");

        /*STYLE*/
        $("#record").removeClass(Chat_config.theme_color).find("i").text("mic_off");

        return;

    },

    upgrade: function () {

        showInfo('Trình duyệt không hỗ trợ Voice :(');
    }

}








