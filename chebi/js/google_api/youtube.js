/**
 * Created by donhatquang on 8/25/17.
 */
var Youtube = {

    player: {},
    done: false,

    /*PLAY VIDEO*/
    youtubePlay: function (obj, type) {

        // var url = $(obj).attr("data-url");

        var type = type || "obj";

        if (type == "id") {
            var videoID = obj;

        }
        else {
            var videoID = $(obj).attr("data-id");

        }


        //console.log(id);
        $("#youtubeModal .modal-content").html('<div id="player"></div>')

        this.player = new YT.Player('player', {

            height: 'auto',
            width: '100%',
            videoId: videoID,

            events: {
                'onReady': this.onPlayerReady,
                'onStateChange': this.onPlayerStateChange
            }
        });

        return;
    },

// 4. The API will call this function when the video player is ready.
    onPlayerReady: function (event) {

        $("#youtubeModal").modal({

            dismissible: false, // Modal can be dismissed by clicking outside of the modal
            // height: '390px';

            // Callback for Modal open.
            ready: function () {

                /*PLAY VIDEO*/
                event.target.playVideo();

                $("#player").css({

                    "position": "absolute",
                    "height": "100%"
                });
            },

            // Callback for Modal close
            complete: function () {

                $("#youtube_btn").show();
                // Youtube.stopVideo();
            }

        }).modal("open");

        return;
    },

    openYoutubeModal: function () {

        $("#youtubeModal").modal("open");

        return;
    },

    // 5. The API calls this function when the player's state changes.
    //    The function indicates that when playing a video (state=1),
    //    the player should play for six seconds and then stop.

    onPlayerStateChange: function (event) {

        var done = this.done;

        /*if (event.data == YT.PlayerState.PLAYING && !done) {
         setTimeout(this.stopVideo, 6000);
         this.done = true;
         }*/
    },

    stopVideo: function () {
        this.player.stopVideo();
    },

    /*LIST YOUTUBE VIDEO FROM DATA*/
    youtubeList: function (data) {

        console.log(data);

        var text = '<ul class="collection Youtube">';

        /*DURATION FIX*/
        var fixduration = function (text) {

            text = text.replace("PT", "");
            text = text.replace("M", ":");
            text = text.replace("H", ":");
            text = text.replace("S", "");

            return text;
        };

        /*LIST YOUTUBE LIST*/
        data.forEach(function (item) {

            /*BASIC INFO*/
            var title = item.title;
            var img = item.thumbnail;
            var link = 'https://www.youtube.com/watch?v=-' + item.id;

            /*STATISTIC RATING*/
            var like = item.likeCount;
            var dislike = item.dislikeCount;
            var view = item.viewCount;

            /*DETAIL*/
            var duration = item.duration;
            var quality = item.quality;
            var videoID = item.id;

            /*AVATAR PICTURE*/
            text += '<li class="collection-item avatar">' +
                '<img src="' + img + '" alt="" class="circle large" style="width: 60px; height: 60px; left: 5px;">';

            /*DURATION*/
            text += '<span class="orange duration">' + fixduration(duration) + '</span>';

            /*QUALITY*/
            if (quality == "hd")
                text += '<i class="material-icons small" style="color: red">hd</i>';


            /*TITLE*/

            text += '&nbsp;&nbsp; <a href="#!" onclick="Youtube.youtubePlay(this);" data-id="' + videoID + '"><span class="title">' + title + '</span></a>';


            /*LIKE * DISLIKE*/
            text += '<div style="margin-top:5px;"><span style="color: orange;"><i class="material-icons">thumb_up</i> (' + numberWithCommas(like) + ')</span>';
                // '&nbsp;&nbsp; <i class="material-icons">thumb_down</i> (' + numberWithCommas(dislike) + ')';

            text += '</div>';

            /*VIEW COUNT */
            text += '<a href="#!" class="secondary-content">' +
                '<strong>' + numberWithCommas(view) + '</strong><i class="material-icons">visibility</i></a>';

        });

        text += "</ul>";

        /*AUTO PLAY THE FIRST*/
        this.youtubePlay(data[0].id, "id");

        return text;


    }



}
