/**
 * Created by donhatquang on 9/1/17.
 */
/*CLASS MAP INIT*/
var mapInit = {

    searchBox: "",

    /*create marker*/
    createMarkers: function (place) {
        // var placeLoc = place.geometry.location;

        //var bounds = new google.maps.LatLngBounds();

        if (place.photos != undefined && place.photos.length != 0) {
            var photo = place.photos[0];
            var img = photo.getUrl({'maxWidth': 100, 'maxHeight': 50});
        }
        else {
            img = place.icon
        }


        /*DRAW ICON*/
        var image = {
            // url: place.icon,
            url: img,

            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
        };

        var marker = new google.maps.Marker({
            map: Gmap.map,
            icon: image,
            position: place.geometry.location
        })
        Gmap.markers.push(marker);


        var infowindow = Gmap.infowindow;
        var infoContent = this.placeDetail(place);
        infoContent = "<div class='place_infowindow'>"+infoContent+"</div>";

        /*CLICK EVENT FOR MARKER*/
        google.maps.event.addListener(marker, 'click', function () {

            infowindow.setContent(infoContent);
            infowindow.open(map, this);
        });

        /*BOWER*/


        return marker.position;
    },

    /*RATE TO STAR ICON*/
    ratingInit: function (rate) {

        /*RATING*/
        //var rate = Math.round(place.rating);
        var text = "";

        /*RATING */
        text += '<a href="#!" class="secondary-content">';
        for (a = 0; a < rate; a++)
            text += '<i class="material-icons">grade</i>';
        text += '</a>';

        return text;
    },

    /*PLACE CONTENT*/
    placeDetail: function (place) {

        var link = "https://www.google.com/maps/search/?api=1&query=" + place.geometry.location + "&query_place_id=" + place.place_id;
        var text = "";
        var img = "";
        var address = place.vicinity || place.formatted_address;

        /*2 TYPE TO GET ADDRESS*/
        // address = place.formatted_address;

        var rating = place.rating || 1;

        /*DISPLAY IMAGE*/
        if (place.photos != undefined && place.photos.length != 0) {
            var photo = place.photos[0];
            // var img = "https://maps.googleapis.com/maps/api/place/photo?"+place.reference+"&key="+Gmap.apikey;
            img = photo.getUrl({'maxWidth': 100, 'maxHeight': 50});
        }

        /*DIS*/
        if (img == "") {
            text = '<i class="material-icons circle large red">location_on</i>'
        }
        else {
            text = '<img src="' + img + '" alt="" class="circle large place_img"> ';

        }

        /*PLACE TITLE AND ADDRESS*/
        text += '<a href="' + link + '" target="_blank" class="place"><span class="title">' + place.name + '</span></a> ' +
            '<p>' + address + ' (Rating: ' + place.rating + ')</p>';

        /* if (place.opening_hours != undefined) {
         text += '<p>Giờ mở cửa:' + place.opening_hours.open_now ? "Mở cửa" : "Đóng cửa" + '</p>';
         };*/

        /*DISTANCE CALCULATE*/
        var currentLocate = new google.maps.LatLng(Gmap.currentLocate);
        var placeLocate = place.geometry.location;

        var distance = google.maps.geometry.spherical.computeDistanceBetween(currentLocate, placeLocate);
        distance = numberWithCommas(Math.round(distance));
        // console.log(distance);

        text += '<p>Khoảng cách: ' + (distance) + 'm</p>';

        /*RATING*/
        text += this.ratingInit(rating);

        return text;
    },

    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    render: function (places) {

        var places = places || mapInit.searchBox.getPlaces();
        var bounds = new google.maps.LatLngBounds();

        if (places.length == 0) {
            return;
        }

        /*RATING STAR INIT*/
        var text = '<ul class="collection">';

        console.log(places);

        /*CLEAR MARKER*/
        Gmap.clearMarker();

        /*LIST ALL OF PLACE*/
        places.forEach(function (place) {

            /*HO HAVE GEOMETRY LOCATION*/
            if (!place.geometry) {

                console.log("Returned place contains no geometry");
                return;
            }

            /*MARKER*/
            /*MARK ON MAPS*/
            var bound = mapInit.createMarkers(place);
            bounds.extend(bound);

            text += '<li class="collection-item avatar">';

            /*DETAIL INIT*/
            text += mapInit.placeDetail(place);
            text += '</li>';

            //console.log(place);
            console.log(place.name + " --> " + place.geometry.location);
        });

        /*SET BOUND CENTER*/
        // console.log(bounds);
        Gmap.map.fitBounds(bounds);

        text += "</ul>";

        /*FULLSCREEN BUTTON*/
        var btn = $("#map_btn_wrap").html();
        text += "<div class='row center'>" + btn + "</div>"


        /*ADD CHAT*/
        Chat.addChat("simi", text, "finish");

        return;

    },

    /*RENDER INPUT BOX TO MAP*/
    renderInputBox: function () {

        var map = Gmap.map;

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);

        $("#pac-input").show();

        /*current location*/
        searchBox.setBounds(map.getBounds());

        /*SEARCH*/
        searchBox.addListener('places_changed', function () {

            // var place = $("#pac-input").val();
            // Gmap.nearbySearch(place);

            /*get places*/
            mapInit.render();
            Gmap.centerMap(13);

            // console.log(place);
        });


        /*---------*/

        /*LET GMAP CONTROL SEARCH BOX*/
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);


        // $('#pac-input').on("")
        /*---------*/

        // Bias the SearchBox results towards current map's viewport.
        mapInit.searchBox = searchBox;

        map.addListener('bounds_changed', function () {

            mapInit.searchBox.setBounds(map.getBounds());
        });

        return;

    }
};
