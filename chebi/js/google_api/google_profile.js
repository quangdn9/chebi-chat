/**
 * Created by donhatquang on 9/18/17.
 */
/*GOOGLE USER PROFILE*/
var G_profile = {

    param: {
        'apiKey': 'AIzaSyC_iuD-B54Y8tQKy5_L19w4zlLn3nwl-Ag',
        // Your API key will be automatically added to the Discovery Document URLs.
        'discoveryDocs': ['https://people.googleapis.com/$discovery/rest?version=v1'],

        // clientId and scope are optional if auth is not required.
//            'clientId': '897401007778-e9fimklm93qo14s86hk0b4dknl0ff61s.apps.googleusercontent.com',
        'clientId': '897401007778-3pj8s0cg90k07gpb7mg4rul3pehl07d4.apps.googleusercontent.com',
        'scope': 'https://www.googleapis.com/auth/userinfo.profile'


    },

    __construct: function () {

        gapi.load('client', this.__init);
    },

    __init: function() {

        // 2. Initialize the JavaScript client library.


        var param = G_profile.param;

        console.log(param);

        gapi.client.init(param).then(function () {

            // 3. Initialize and make the API request.
            return gapi.client.people.people.get({
                'resourceName': 'people/me',
                'requestMask.includeField': 'person.names,person.genders,' +
                'person.ageRange,' +
                'person.biographies,' +
                'person.interests,' +
                'person.birthdays,' +
                'person.nicknames'
            });

        }).then(function (response) {

            console.log(response.result);

        }, function (reason) {

            console.log('Error: ');
            console.log(reason);
        });
    }
}