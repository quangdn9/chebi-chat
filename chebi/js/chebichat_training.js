/**
 * Created by donhatquang on 11/28/17.
 */
var Training = {

    /*Training for chebi*/
    trainingModal: function (obj) {

        var bubble = $(obj).parents(".chat_content").find(".bubble");

        bubble.find(".pinyin").remove();

        var text = bubble.text();

        console.log(text);

        $(".chebi-training-source").val(text);

        /*TRAINING*/
        $("#trainingModal").modal("open");

        return;
    },

    trainingSend: function () {

        var param = {

            uid: Chebi_user.userinfo.uid,
            pageid: page,
            keyword: $("#chebi-training-source").val(),
            reply: $("#chebi-training-reply").val(),
            devmode: devmode
        }


        $.getJSON("chebi_training.php", param, function (data) {

            console.log(data);

            if (data.result == "successful") {

                Materialize.toast("Lưu trữ thành công", 2000);
            }

            else {

                Materialize.toast("Lưu trữ thất bại", 2000);
            }

        })

        return false;
    }
}