<?php
/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 8/21/17
 * Time: 11:43 AM
 */
require("api/translator.php");
require("api/AipHttpClient.php");


if (isset($_GET["target"])) {

    $target = $_GET["target"];
}
else {
    $target = "zh";
}


/*SEARCH*/
if (isset($_GET["content"]) && isset($_GET["lang"])) {

    $source = $_GET["lang"];
    $keyword = $_GET["content"];


    $http = new AipHttpClient();


    $translator = new Translator($keyword, $http);



    $data = $translator->translate($target);

    $result = array(

        "result" => $data
    );

    echo json_encode($result);

    return;
}

?>
