<?php
/*
 * Author: Do Nhat Quang
 * Chebi Chat Main Function
 * */
//header("Content-type: text/html; charset:utf-8");
header("Content-type: application/json; charset:utf-8");


define("ROOT", $_SERVER["DOCUMENT_ROOT"]);

require(ROOT . "/db_conn.php");
require(ROOT . "/includes/api_config.php");


/*-------CHEBI API------*/
/*CHEBI QUERY CLASS*/
require("api/AipHttpClient.php");
require_once("api/translator.php");


/*CHEBI APP CONTROLLER*/
require_once("app/app_controller.php");

/*sensitive analyze*/
require("api/sensitive.php");

/*CHEBI APP*/
require("chebi_get_data.php");
require("app/Chebi_app.php");

/*SET UP APP CONTROLLER*/
$Chebi = new app_controller();

//var_dump($_SERVER);
//exit();

/*PARAM VALUE*/
$lc = $_GET["lc"];
$keyword = trim(strtolower($_GET["reqText"]));


/*------------*/
if (!isset($_GET["reqText"]) || !isset($_GET["lc"])) {

    return "false";
}

/*FANPAGE ID*/
$pageid = "";
if (isset($_GET["pageid"]) && $_GET["pageid"] != "") {

    $pageid = $_GET["pageid"];
}

/*CHINESE*/
$chinese = false;
if ($lc == "zh" || $lc == "hk") {

    $chinese = true;
}

/*DEV MODE*/
$response = "";
$property = "";

$result = false; /*PRINT THE RESULT*/

try {

    /*DEFINE APP*/
    $ChineseApp = new tiengtrung_app();

    /*CHECK APP KEYWORD*/
    $check_App = $Chebi->checkApp(strtolower($keyword), $lc);

//    var_dump($check_App);
//    exit();

    /*IF SIMSIMI CALL APP TO DISPLAY*/
    if ($check_App) {

        /*CHECK IF APP TYPE (array) -- MINI APP IN LOCAL AS 成语, 食品*/
        if ($check_App["local"] == true) {

            /*FIND RESPONSE FROM LOCAL DATABASE*/
            $ChineseApp->setDbpic($servername, $username, $password, $db_piwigo); /*define conn to piwigo database*/


            /*GET LOCAL */
            $result = get_local_data($check_App, $ChineseApp);

//            exit();
        } /*CHEBI App*/
        else {

//            echo "Chebi app";

//            var_dump($check_App);

            $Chebi_app = new Chebi_app($check_App, $lc);

            $result = $Chebi_app->check_app();


        }
    } /*IT'S NOT APP TYPE*/
    else {

        $except_word = array("新闻", "娱乐");

        $flag = false;
        foreach ($except_word as $word) {

            if (strpos($keyword, $word) !== false) $flag = true;
        }

        /*NOT INCLUDE EXCEPT WORD -> LOCAL*/
        if ($flag == false) {

            /*CHAT TEXT WITH BOT*/
            $result = $Chebi->getChatResponse($keyword, $pageid, $lc);

            $response = "";
            if ($result) {

                $response = $result->{"reply"};
            }

            $result = array(

                "response" => $response,
                "msg" => "OK",
                "source" => "Chebi from Local"
            );
        }

    }

    /*---------------*/
    /*NO RESULT --> REQUEST FROM SIMSIMI DB*/
    $filter = false;

    /*GET DATA FROM SIMSIMI API*/
    if ($result["response"] == "") {


        /*GET SIMSIMI*/
        $lc = "zh";
        $result = get_remote_chat($keyword, $lc);


    }


    /*ADD PINYIN*/
//    if ($chinese && isset($result["property"]) == false) {
    if ($chinese && $result["response"] != "") {

        $pinyin = $ChineseApp->convertPinyin($result["response"]);
        $result["pinyin"] = $pinyin;
    }

    /*SET LANG TO FRONT-END*/
    $result["language"] = $lc;


    /*OUTPUT*/
    echo json_encode($result);


} catch (Exception $e) {

    echo $e->getMessage();
}