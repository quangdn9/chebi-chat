<?php

/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 8/16/17
 * Time: 10:59 AM
 */

require("youtube.php");
require("api/wiki_api.php");
require("api/taobao_api.php");

/*CHINESE FEATURE API*/
require("api/chinese_api.php");

use Overtrue\Pinyin\Pinyin;

class tiengtrung_app
{
    //private $conn;
    private $app;

    private $input;
    private $dbpic;

    private $pichost = "https://thuvien.tiengtrung.co";

    /**
     * @return mixed
     */
    public function getDbpic()
    {
        return $this->dbpic;
    }

    /**
     * @param mixed $dbpic
     */
    public function setDbpic($servername, $username, $password, $db_piwigo)
    {
        $dbpic = connectdb($servername, $username, $password, $db_piwigo);
        $this->dbpic = $dbpic;
    }

    /*SET PARAM FROM CHICKEN*/
    public function setParam($app, $input = "")
    {
        $this->app = $app;
        $this->input = $input;
    }

    /*GET DATA FROM DB*/
    public function getData($type, $keyword = "")
    {

//        var_dump($type);


        $this->input = $keyword;

        $sql = "";

        if ($type == "foods") {

            $sql = "
                SELECT FoodAlbum.Cat,
                    FoodAlbum.Title,
                    FoodAlbum.Image
                FROM FoodAlbum
                WHERE FoodAlbum.Title LIKE '%$this->input%'

         ";


        } else if ($type == "story") {


            $word_cat = array("类", "分类", "篇", "片", "种类");


            $cat = false;

            /*SEARCH CAT ELEMENT*/
            foreach ($word_cat as $cat_value) {

                if (strpos($keyword, $cat_value) !== false) {

                    list($cat, $title) = explode($cat_value, $keyword);
                }
            }

            /*HAVE CAT*/
            if ($cat == true) {


                $sql = "SELECT * FROM `comedy_content`,`comedy_cat` WHERE

          `comedy_content`.`标题` LIKE '%" . $title . "%'
          AND `comedy_cat`.`分类ID`= `comedy_content`.`分类ID`
          AND `comedy_cat`.`分类名称` LIKE '%" . $cat . "%'
          
          ";


            } else {


                $sql = "SELECT * FROM `comedy_content`,`comedy_cat` WHERE

          `comedy_content`.`标题` LIKE '%" . $this->input . "%'
          AND `comedy_cat`.`分类ID`= `comedy_content`.`分类ID`
          
          ";
            }

//AND `序号` LIKE '18337'

        } else if ($type == "idiom") {

            $sql = "SELECT
            
            IdiomPicture.picture,
            IdiomExplain.*
        FROM IdiomPicture, IdiomExplain
        WHERE IdiomExplain.cyname LIKE IdiomPicture.cyname 
        AND IdiomExplain.cyname LIKE '%" . $this->input . "%'";

        }

        /*GET DATA*/
//        echo $sql;
//        echo "<hr/>";

        $sql .= " ORDER BY RAND() LIMIT 1";

        $data = $this->getRandom($sql);
//        echo $data->{"内容"};
//        var_dump($data);
//        exit();

        /*FIX STORY*/
        if ($type == "story") {

            $data->{"内容"} = $this->fixInitStory($data->{"内容"});
        }

        return $data;

    }

    public function fixInitStory($text)
    {

        $text = nl2br($text);


//        exit();
//            $text = str_replace("。","。<br/>",$text);

//        echo $text;
//        echo "<hr>";

        $data = explode("<br />", $text);
        $data = array_filter($data);

//        echo count($data);

        $tmp = array();

        foreach ($data as $key => $row) {

            if (strlen($row) > 1) {

                $row = str_replace("　", "", $row);
                $row = trim(preg_replace('/\t+/', '', $row));

                $tmp[] = $row;
            }

        }
        $text = implode("|", $tmp);

//        $text = str_replace("</br>","<br/>",$text);

        return $text;
    }

    public function getImages($name)
    {

        $sql = "SELECT id,path FROM `piwigo_images` WHERE `name` LIKE '%" . $name . "%' ORDER BY `id` DESC LIMIT 0,1";

        $result = $this->dbpic->query($sql);
        $data = "";

        if ($result->num_rows > 0) {

            $data = $result->fetch_object();

            $result = array(

                "image" => str_replace('./', '/', $this->pichost . $data->{"path"}),
                "url" => $this->pichost . "/picture.php?/" . $data->{"id"},
                "type" => "picture"

            );

            return $result;

        }

        return false;
    }

    /*GET RANDOM RECORD FROM DATABASE*/
    public function getRandom($sql)
    {

//        echo $sql;

        $result = $GLOBALS["conn"]->query($sql);
        $data = array();

//        echo ($result->num_rows);
//        var_dump($result);

        if (!empty($result) && $result->num_rows > 0) {


            $data = $result->fetch_object();
//            var_dump($data);
        }

        return $data;
    }

    /*get youtube app*/
    public function getYoutube($keyword, $pageToken = false)
    {

        $youtube = new Youtube($keyword);

        return $youtube->search($pageToken);
    }

    /*GET DATA FROM WIKIPEDIA*/

    public function getWiki($keyword, $lang)
    {
        /*DEFINE WIKI OBJECT*/
        $wiki = new Wiki($keyword, $lang);

        $data = $wiki->search();

        return $data;

    }

    /*GET STROKE CHINESE*/
    public function getChineseStroke($keyword)
    {

        $chinese = new Chinese_api($keyword);
        $result = $chinese->getStroke();

        return $result;
    }

    public function getTaobaoKeyword($keyword)
    {

        /*get taobao*/

        $Taobao = new Taobao_api($keyword);

        $translate = $Taobao->getKeyword(array("zh-CN"));

        return $translate;

    }

    /*CONVERT TO PINYIN*/
    public function convertPinyin($word)
    {

        // 小内存型
        $PINYIN = new Pinyin(); // 默认
        $result = [];

        /*REMOVE HTML*/
        $word = strip_tags($word, "");

        if (strpos($word, "|") !== false) {

            $data = explode("|", $word);

            foreach ($data as $words) {

                $words = $PINYIN->convert($words,PINYIN_UNICODE);
                /*var_dump($words);
                exit();*/


                $result = array_merge($result,$words,["|"]);

            }
//            var_dump($result);
//            exit();

        } else
            $result = $PINYIN->convert($word, PINYIN_UNICODE);

        $result = implode(" ", $result);

        return $result;

    }
}