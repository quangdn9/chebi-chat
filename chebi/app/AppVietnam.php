<?php

/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 8/24/17
 * Time: 5:26 PM
 */
class AppVietnam
{

    private $input;

    private $verbVN = array(

        /*spec word for google map*/
        "map" => array("quanh", "gần", "đến", "chơi", "ăn", "uống", "bán", "nghỉ", "tập", "nhuộm", "giặt")

        /*EXternal for smarthome*/
//        "smarthome" => array("bật", "tắt")
    );

    /*PUSH THE LAST INDEX NUMBER FROM THE WORD*/
    public function fixWordfromDEP($noun)
    {

        /*cafe-3, điều-hòa-2*/
        $noun = explode("-", $noun);
        array_pop($noun);

        /*implode by "-" */
        $noun = implode(" ", $noun);

        return $noun;
    }

    /*GET NOUN FROM WORDS*/
    public function getNounbyVerbVN($tag, $data = array())
    {
        $result = array(

            "noun" => "",
            "verb" => "",
            "verbType" => "" /*spec_word_front, spec_word_back v.v...*/
        );


        /*"dobj"*/
        foreach ($data as $item) {

            /*dobj*/
            if (strpos($item, $tag) !== false) {


                list(, $item) = explode($tag, $item);

                $item = str_replace(["(", ")"], "", $item);

                list ($verb, $noun) = explode(",", $item);

                /*GET & FIX NOUN*/
                $noun = $this->fixWordfromDEP($noun);
                $verb = $this->fixWordfromDEP($verb);

                $result["noun"] = $noun;
                $result["verb"] = $verb;
                $result["words"] = $verb . " " . $noun;

                /*SEARCH FOR VERB*/
                foreach ($this->verbVN as $key => $verbs) {

                    $checkverb = array_search($verb, $verbs);

                    if ($checkverb) {

                        $result["verbType"] = $key;
                    }
                }
            }
        }

        return $result;
    }

    /*GET TAG FOR WORDs*/
    public function getTags($data = array())
    {

        $words = array();

        /*cửa-hàng/N quần-áo/N gần/A đây/P */

        /*LIST ARRAY by Space */
        foreach ($data as $item) {

            list($text, $tag) = explode("/", $item);

            /*GET NOUN FOR SEARCH GOOGLE MAP PLACE*/
            if ($tag == "N") {

                $text = str_replace("-", " ", $text);

                $word = array(

                    "text" => $text,
                    "tag" => $tag
                );
                $words[] = $word;
            }
        }

        return $words;
    }

    /*USE FPT API for analyze words*/
    public function FPT_analyze($current)
    {

        /*BEGIN CHECK IF VERB CONDITION TRUE*/
        //  $url = FPT_AI_URL . "/dep/";

        $url = FPT_AI_URL . "/tag/";
        $header = array(
            "api_key" => FPT_API,
        );

        /*dev mode*/
        if (isset($_GET["devmode"]) && $_GET["devmode"] == "true") {

            /*TAG*/
            /*root(ROOT-0,đi-1) vv(đi-1,uống-2) dobj(uống-2,cafe-3)
            root(ROOT-0,bật-1) dobj(bật-1,điều-hòa-2)
            cửa-hàng/N quần-áo/N gần/A đây/P
            */

            $result = array(

                "code" => 200,
                "content" => 'cửa-hàng/N quần-áo/N gần/A đây/P'
            );

        } else {

            $http = new AipHttpClient($header);
            $result = $http->post($url, ($this->input));

        }

        /*GET RESULT*/
        if ($result["code"] == 200) {

            $content = $result["content"];

            /*CHECK VERB - NOUN*/
            /*if (strpos($content, "dobj")) {

                $content = explode(" ", $content);
                $content = $this->getNounbyVerbVN("dobj", $content);

                return $content;
            }*/


            $content = explode(" ", trim($content));

            /*GET NOUN LIST*/ /*CHECK TAG OF WORDS*/
            $content = $this->getTags($content);

            /*var_dump($content);
            exit();
            */
            if (count($content) == 0) return false;

            /*GET THE LAST ONE of noun*/
            $content = array_pop($content)["text"];

            /*PRINT RESULT*/
            $result = array(

                "keyword" => $content,
                "type" => $current["type"]
            );

            return $result;
        }

        return false;
    }

    /*CHECK IF THE WORD FOR APP*/
    public function checkAppVN()
    {

        /*CHECK Verb type first (Map or Smarthome)*/
        foreach ($this->verbVN as $key => $verbs) {

            /*EACH VERB*/
            foreach ($verbs as $verb_item) {

//              echo $this->input . " -> " . $verb_item . "<br/>";

                /*SEARCH TYPE OF VERB*/
                if (strpos($this->input, $verb_item) !== false) {

                    $current = array(

                        "verb" => $verb_item,
                        "type" => $key /*type of verb*/
                    );

                    /*NORMALLY SEARCH BY NOUN from FPT*/
                    $result = $this->FPT_analyze($current);

                    //----------

                    /*PRINT RESULT*/

                    return $result;
                }
            }
        }

      return false;

    }


    public function __construct($input)
    {

        $this->input = $input;
    }


}