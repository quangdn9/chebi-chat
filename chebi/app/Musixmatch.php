<?php
/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 13/10/18
 * Time: 5:49 PM
 */

class Musixmatch
{

    private $url = "";
    private $http;
    private $api_key = "fb276d006691dbbdf5af677fc05ffcf2";


    public function __construct()
    {
        $this->http = new AipHttpClient();
        $this->url = "https://api.musixmatch.com/ws/1.1/";
    }

    public function searchLyric($kwd)
    {

        $param = array(

            "format" => "jsonp",

            "callback" => "callback",
            "s_track_rating" => "desc",
            "s_artist_rating" => "desc",
            "f_has_lyrics" => 1,

            "q_track" => $kwd,
            "quorum_factor" => 1,
            "apikey" => $this->api_key
        );

        $data = $this->http->get($this->url . "track.search", $param);

//        var_dump($data);
//        exit();

        $result = false;

        if ($data["code"] == 200) {


            $result = json_decode($this->fix_result($data["content"]), false);

            $track_list = $result->message->body->track_list;

            $result = array();

//            var_dump($track_list);
//            exit();

            foreach ($track_list as $track) {


                $track = $track->track;

                /*OUTPUT*/
                $item = array(

                    "track_id" => $track->track_id,

                    "track_name" => $track->track_name,
                    "artist_name" => $track->artist_name,
                    "album_name" => $track->album_name
                );

                $result[] = $item;
            }

//            $track_id = $track->track_id;

//            var_dump($track);

            /*GET THE FIRST TRACK LYRIC*/
//            $lyric = $this->getLyric($result[0]["track_id"]);
            $result[0]["lyric"] = "";

        }

        return $result;
    }

    public function fix_result($result)
    {

        $result = str_replace(array("callback(", ");"), "", $result);

        return $result;
    }

    /*GET BY TRACK ID*/
    public function getLyric($track_id)
    {

        $param = array(

            "format" => "jsonp",

            "callback" => "callback",

            "track_id" => $track_id,

            "apikey" => $this->api_key
        );

        $data = $this->http->get($this->url . "track.lyrics.get", $param);

        $result = json_decode($this->fix_result($data["content"]), false);

        $lyric = "";
        if (count($result->message->body) != 0) {

            $lyric = $result->message->body->lyrics->lyrics_body;
        }


//        var_dump($result->message->body);
//        exit();

        return $lyric;
    }

}