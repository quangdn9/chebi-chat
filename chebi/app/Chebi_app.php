<?php

/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 4/5/18
 * Time: 13:51
 */

/*CHINESE APPLICATION API*/
require("tiengtrung_app.php");

require_once("DepositPhoto.php");
require_once("Musixmatch.php");

//require("Smarthome.php");


class Chebi_app
{

    /*RESULT FROM RULE FILTER*/
    private $result;
    private $lc; /*LANGUAGE*/
    private $app_name;
    private $app_type;

    private $tiengtrung_app;

    private $app;


    public function __construct($result = "", $lc = "")
    {

        if ($result != "") {

            $this->result = $result;
            $this->lc = $lc;

            $this->app_type = $result["app_type"];
            $this->app_name = $result["keyword"];

        }

        $this->tiengtrung_app = new tiengtrung_app();

        return;
    }


    /*SET APP INFORMATION*/
    public function setApp($response, $property, $source, $ad = false)
    {

        /*OUTPUT*/
        $app = array(

            "response" => $response,
            "msg" => "OK",
            "property" => $property,
            "source" => $source
        );

        /*SET ADVERTISING*/
        /*if ($ad == true) {

            $app["ad"] = array(

                "keyword" => $this->app_name,
                "type" => "admob"
            );
        }*/

        $this->app = $app;

        return;
    }

    /*GET APP INFO*/
    public function getApp()
    {

        /*OUTPUT*/
        return $this->app;
    }

    /*CHECK APP*/
    public function check_app()
    {

        $app_type = $this->app_type;

        switch ($app_type) {

            /*YOUTUBE*/
            case "youtube":

                $this->youtube_app();

                break;
            /*WIKI INIT*/
            case "wiki":

                $this->wiki_app();
                break;
            /*MAP*/
            case "map":

                $this->map_app();
                break;
            /*SEARCH TAOBAO*/
            case "taobao":

                $this->taobao_app();
                break;

            case "writing":

                $this->chinese_writing();
                break;

            case "photos":

                $this->photo_list();

                break;

            case "lyric":
                $this->get_lyric();

                break;

        }


        /*CONTROL SMARTHOME*/
        /*else if ($app_type == "smarthome") {

            return $this->smarthome();

        }*/

        /*OUTPUT RESULT*/
        $App_output = $this->app;

        return $App_output;
    }

    /*LYRIC*/
    public function get_lyric()
    {

        $kwd = $this->result["keyword"];

        $MusixMatch = new Musixmatch();

        $lyric = $MusixMatch->searchLyric($kwd);

//        var_dump($lyric);
//        exit();

        $response = "Lyric for " . $this->app_name;
        $source = "Lyric";

        $property = array(

            "type" => "lyric",
            "list" => $lyric

        );

        /*SET APP*/
        $this->setApp($response, $property, $source, true);

        return $lyric;

    }

    /*PHOTO LIST*/
    public function photo_list()
    {

        $kwd = $this->result["keyword"];

        $response = "Photos list for " . $this->app_name;
        $source = "Stock photos";

        $Photo = new DepositPhoto();

        $param = array(

            "title", "description", 'width', 'height', 'views', 'published', 'updated',
            'categories', 'thumbnail', 'thumb_max'
        );

        /*CHECK PAGE*/
        if (isset($_GET["pageid"]) && $_GET["pageid"] != "") {

            $Photo->setPage($_GET["pageid"]);
        }

        $list = $Photo->getPhoto($kwd, "zh", $param);

//        var_dump($list);
//        exit();

        $_GET["pageid"] = $Photo->getPage() + 1;
        $nextpage = http_build_query($_GET);

        $property = array(

            "type" => "photos",
            "list" => $list,
            "nextpage" => $nextpage

        );

        /*SET APP*/
        $this->setApp($response, $property, $source, true);


        return;
    }

    /*CHINESE WRITING APP*/
    public function chinese_writing()
    {

        $kwd = $this->result["keyword"];

        $stroke = $this->tiengtrung_app->getChineseStroke($kwd);

        $response = "Pictures list for " . $this->app_name;
        $source = "Chinese Stroke";

        $property = array(

            "type" => "stroke",
            "list" => $stroke

        );

        /*SET APP*/
        $this->setApp($response, $property, $source, true);

        return;
    }

    /*YOUTUBE APP*/
    public function youtube_app()
    {


        $response = "Youtube list for " . $this->app_name;
        $source = "Youtube";

        /*PAGE TOKEN*/
        $nextPage = false;

        if (isset($_GET["nextPage"])) {

            $nextPage = $_GET["nextPage"];
        }

        /*GET YOUTUBE LIST DATA*/
        $app = $this->tiengtrung_app;

        $youtubeData = $app->getYoutube($this->app_name, $nextPage);

        /*SETUP PROPERTY */
        $property = youtube_init($youtubeData);

        /*SET APP*/
        $this->setApp($response, $property, $source, true);

        return;

    }

    /*WIKI*/
    public function wiki_app()
    {

        $response = "Wikipedia for " . $this->app_name;
        $source = "Wikipedia";

        /*GET WIKI LIST DATA*/
        if (DEVMODE) {

            $data = file_get_contents("./app/wiki_search.json");

        } else {

            $data = $this->tiengtrung_app->getWiki(trim($this->app_name), $this->lc);
            $data = $data["content"];

        }

        $lang = "vn";
        /*CHECK CHINESE*/
        if (preg_match("/\p{Han}+/u", $this->app_name) != 0) {
            $lang = "zh";
        }
        /*CHECK LANG*/

        $property = wiki_init($data);
        $property["lang"] = $lang;

        /*SET APP*/
        $this->setApp($response, $property, $source, true);

        return;
    }

    /*MAP*/
    public function map_app()
    {

        $property = array(

            "type" => "map",
            "keyword" => $this->result["keyword"]
        );

        $response = "Map for " . $this->app_name;
        $source = "App for map";

        /*SET APP*/
        $this->setApp($response, $property, $source);

        return;
    }

    /*TAOBAO*/
    public function taobao_app()
    {

        $property = array(

            "type" => "taobao",
            "keyword" => $this->result["keyword"],
            "translate" => $this->tiengtrung_app->getTaobaoKeyword($this->result["keyword"])
        );

        $response = "Taobao " . $this->app_name;
        $source = "App for taobao";

        /*SET APP*/
        $this->setApp($response, $property, $source);

        return;
    }

    /*SMARTHOME*/
    /*public function smarthome()
    {

//        CHECK STATUS OF DEVICE
        $status = isset($_GET["status"]) ? $_GET["status"] : "on";

        $smarthome = new Smarthome($status);

        $output = $smarthome->checkAction($this->result);

        return $output;
    }*/

    public function setTiengtrung_app($conn)
    {

        $this->tiengtrung_app = new tiengtrung_app($conn);

        return;

    }

    public function getTiengtrung_app()
    {

        return $this->tiengtrung_app();
    }
}