<?php
if (!file_exists(__DIR__ . '/vendor/autoload.php')) {
    throw new Exception('please run "composer require google/apiclient:~2.0" in "' . __DIR__ . '"');
}

require_once __DIR__ . '/vendor/autoload.php';

use Google\Cloud\Translate\TranslateClient;

// $text = 'The text whose language to detect.  This will be detected as en.';


/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 11/13/17
 * Time: 10:54 AM
 */
class Langapp
{
    private $keyword;
    private $translate;

    # Your Google Cloud Platform project ID
    private $projectId ="tiengtrung-177206";

    public function __construct($keyword)
    {

        $this->keyword = $keyword;

        $this->translate = new TranslateClient([
            'projectId' => $this->projectId
        ]);

    }

    public function detect () {

        $text = $this->keyword;

        $result = $this-> translate->detectLanguage($text);

        print("Language code: $result[languageCode]\n");
        print("Confidence: $result[confidence]\n");

        return;

    }
}

$translator = new Langapp("xin chào");

echo $translator->detect();