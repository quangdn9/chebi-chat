<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 9/7/2018
 * Time: 12:28 AM
 */

class DepositPhoto
{
    private $url = "";
    private $http;
    private $page = 0;

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    public function __construct()
    {
        $this->http = new AipHttpClient();
        $this->url = "https://api.depositphotos.com/";
    }

    public function getPhoto($kwd, $lang = "zh", $element = [], $limit = 20)
    {



//        exit();

        $param = array(

            "dp_apikey" => "6ece88f4fcd7e52a9068ea46fe26120d0293cb4f",
            "dp_command" => "search",
            "dp_search_query" => $kwd,
            "dp_search_sort" => 1,
            "dp_search_nudity" => 1,
            "dp_search_limit" => $limit,

            "dp_search_offset" => $this->page*$limit,



        );


        /*CHECK CHINESE*/
        if (preg_match("/\p{Han}+/u", $kwd) != 0) {

            $lang_param = array(

                "dp_lang" => $lang,
                "dp_translate_items" => "true"
            );

            $param = array_merge($param, $lang_param);

        }



        $data = $this->http->get($this->url, $param);


        $data = json_decode($data["content"], true);

        if ($data["type"] == "success") {

            $result = $data["result"];

//            echo count($result);
//            exit();

            if (count($element) != 0) {

                $tmp_data = array();

                foreach ($result as $key => $item) {

                    $tmp_data_element = array();

                    /*LIST PARAM TO GET*/
                    foreach ($element as $value) {

                        $tmp_data_element[$value] = $item[$value];
                    }

                    $tmp_data[] = $tmp_data_element;

                    /*if ($limit == true && $key == $limit) {

                        return $tmp_data;

                    }*/

                }
            }

            return $result;
        }

        return false;
    }


}