<?php

/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 8/16/17
 * Time: 9:21 AM
 */

//require("AppVietnam.php");


class app_controller
{
    private $conn;
    private $input;

    public function __construct()
    {
        $this->conn = $GLOBALS["conn"];
    }



    public function checkAppType()
    {

//        $App_type = false;

        $word = array(

            "youtube" => array(

                "spec_word_front" => array(

                    /*COMMON*/
                    "youtube",

                    /*VIETNAM*/
                    "xem", "nghe",

                    /*CHINESE*/
                    "看", "听",

                    /*ENGLISH*/
                    "watch", "listen", "listen to", "video of"


                ),

                "spec_word_back" => array(

                    /*JAPANESE*/
                    "を見る", "ビデオ", "画像"

                )
            ),

            /*"smarthome" => array(

                "spec_word_front" => array(

//                  VIETNAM
                    "bật", "kéo", "tắt", "đóng", "mở", "bat", "tat", "smarthome"
                )
            ),*/

            "wiki" => array(

                "spec_word_front" => array(

                    /*VIETNAM*/
                    "thế nào là", "ai là", "định nghĩa", "tìm kiếm", "thông tin về",

                    /*CHINESE*/
//                    "什么是", "谁是",

                    /*ENGLISH*/
                    "what is",


                ),

                "spec_word_back" => array(


                    /*VIETNAM*/
                    "là gì", "là cái gì", "là ai",

                    /*CHINESE*/
                    "是什么", "是谁",

                    /*ENGLISH*/
                    "on wiki",

                    /*JAPANESE*/
                    "はなに", "とは"


                )
            ),

            "taobao" => array(

                "spec_word_front" => array(

                    /*VIETNAM*/
                    "đặt hàng",

                    /*CHINESE*/
                    "买", "订货", "购物",

                    /*ENGLISH*/
                    "order", "how much is", "buy", "i want"


                ),

                "spec_word_back" => array(

                    /*JAPANESE*/
                    "を買う", "ほしい", "注文", "いくら"


                )
            ),

            "map" => array(

                "spec_word_front" => array(

                    /*VIETNAMESE*/
                    "đi ", "đến", "tìm",

                    /*CHINESE*/
                    "去",

                    /*ENGLISH*/
                    "go to", "find", "map of"
                ),


                "spec_word_back" => array(

                    /*VIETNAMESE*/
                    "gần đây", "quanh đây", "ở đâu", "ở chỗ nào", "chỗ nào",

                    /*CHINESE*/
                    "在哪儿", "在哪", "在哪里",

                    /*JAPANESE*/
                    "へ行く", "行こう", "行け", "のマップ"

                )
            ),

            /*CHINESE STROKE*/
            "writing" => array(

                "spec_word_back" => array(

                    "怎么写", "如何写",

                    /*vietnam*/
                    "viết như nào"
                )
            ),

            /*PHOTO*/
            "photos" => array(

                "spec_word_front" => array(

                    /*VIETNAMESE*/
                    "ảnh của", "ảnh về", "ảnh"

                ),

                "spec_word_back" => array(

                    "的图片", "图片"
                ),
            ),

            /*Lyric*/
            "lyric" => array(

                "spec_word_front" => array(

                    /*VIETNAMESE*/
                    "lời bài hát", "tìm lời bài", "tìm lời"

                ),

                "spec_word_back" => array(

                    "的歌词", "歌词"
                )
            )

        );

        /*CHECK ALL LOCAL TYPE*/
        $local_word = array(

            "story" => array(


                "spec_word_front" => array(

                    "讲故事","听故事"
                ),
                
                "spec_word_back" => array(

                    "的故事", "故事"
                )

            ),

            "idiom" => array(


                "spec_word_back" => array(

                    "的成语", "成语"
                )

            )

        );

        $total = array(

            "local" => $local_word,
            "app" => $word

        );

//        var_dump($total);
//        exit();

        $result = false;

        foreach ($total as $word_key => $word_array) {

            foreach ($word_array as $app_type => $type_define) {

//                var_dump($type_define);

                $result = $this->checkTypeDefine($app_type, $type_define);

                /*MAP, FRONT_BACK*/
                if ($result == true) {

                    if ($word_key == "local") {

                        $result["local"] = true;
                    }

                    else
                        $result["local"] = false;

                    return $result;
                }
            }
        }


        return false;
    }

    /*CHECK IF INPUT IS YOUTUBE APP*/
    public function checkTypeDefine($app_type, $type_define)
    {

        /*FIX UPPER*/
        $this->input = strtolower($this->input);

        /*LIST TYPE OF WIKI WORDS*/
        foreach ($type_define as $type => $words) {

            foreach ($words as $word) {


                /*---------------
                */
                /*have result*/
                $position = strpos($this->input, $word);

                if ($position !== false) {

                    /*SPLIT PART OF WORDS*/
                    list($part1, $part2) = explode($word, $this->input);

                    /*GET PART OF CONTENT*/
                    if ($type == "spec_word_back" && $position != 0) {

                        $keyword = $part1;

                    } else {

                        $keyword = $part2;

                    }

                    $keyword = trim($keyword);

                    /*FILTER*/
                    $keyword = str_replace(array("关于"),"",$keyword);

                    return [
                        "app_type" => $app_type,
                        "keyword" => $keyword,
                        "action" => $word
                    ];


                }
            }

        }

        return false;
    }


    /*CHECK IF CALL THE APP*/
    public function checkApp($input, $lang = "zh")
    {

        /*CHECK EXCEPT WORDS*/
        $except_word = array("接龙");

        foreach ($except_word as $word) {

            /*HAVE EXCEPT WORDS*/
            if (strpos($input, $word) !== false) {

                return false;
            }
        }

        /*CHECK TYPE*/
        $this->input = $input;

        $type = $this->checkAppType();

//        var_dump($type);


        return $type;


    }

    public function getChatResponse($input, $pageid = "", $lang = "vn")
    {

        /*select from 2 table of response*/

        $chat_local_tbl = "SELECT
            chat_local.输入 as input,
            chat_local.机器 as reply
        FROM chat_local
        WHERE chat_local.输入 LIKE '%" . $input . "%'";


        if ($pageid != "") {

            $sql = $chat_local_tbl . " AND chat_local.pageid LIKE '" . $pageid . "' ORDER BY id DESC LIMIT 1";


        } /*NO PAGE ID*/
        else {


            /*FIND WITH CHINESE*/
            if ($lang == "zh" || $lang == "ch") {

                $chat_local_tbl .= " AND chat_local.pageid LIKE NULL ";

                $simsimi_tbl = "SELECT simsimi.input,`simsimi`.`bot` as reply
        FROM simsimi
        WHERE simsimi.input LIKE '%" . $input . "%'";

                $sql = $chat_local_tbl . " UNION ALL " . $simsimi_tbl . " ORDER BY RAND() LIMIT 1";

            } else {

                $sql = $chat_local_tbl . " ORDER BY RAND() LIMIT 1";
            }
        }

        /*CHECK PAGEID*/

        $result = $this->conn->query($sql);

        $data = array();

//        var_dump($result);
//        exit(0);


        if ($result !== false && $result->num_rows > 0) {

            while ($row = $result->fetch_object()) {

                $data[] = $row;

            }

            return $data[0];
        }

        return false;
    }

}