<?php

/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 8/24/17
 * Time: 5:50 PM
 */

/**
 * Library Requirements
 *
 * 1. Install composer (https://getcomposer.org)
 * 2. On the command line, change to this directory (api-samples/php)
 * 3. Require the google/apiclient library
 *    $ composer require google/apiclient:~2.0
 */
if (!file_exists(__DIR__ . '/vendor/autoload.php')) {
    throw new Exception('please run "composer require google/apiclient:~2.0" in "' . __DIR__ . '"');
}

require_once __DIR__ . '/vendor/autoload.php';


class Youtube
{
    private $keyword;
    private $youtube;

    public function __construct($keyword)
    {
        $this->keyword = $keyword;


        $client = new Google_Client();
        $client->setApplicationName('chebichat');
        $client->setDeveloperKey("AIzaSyBb9OPfqAKE25XJy2NwHF4Fi34ZRMKX29E");
       $client->setScopes([
            'https://www.googleapis.com/auth/youtube.readonly',
        ]);
        /*
       // TODO: For this request to work, you must replace
       //       "YOUR_CLIENT_SECRET_FILE.json" with a pointer to your
       //       client_secret.json file. For more information, see
       //       https://cloud.google.com/iam/docs/creating-managing-service-account-keys
               $client->setAuthConfig('client_secret_897401007778-ma12haq91ca8fdfpqkvir9e2b3du8c0l.apps.googleusercontent.com.json');
               $client->setAccessType('offline');

       // Request authorization from the user.
               $authUrl = $client->createAuthUrl();
               printf("Open this link in your browser:\n%s\n", $authUrl);
               print('Enter verification code: ');
               $authCode = trim(fgets(STDIN));

       // Exchange authorization code for an access token.
               $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
               $client->setAccessToken($accessToken);*/

// Define service object for making API requests.
        $this->youtube = new Google_Service_YouTube($client);

        return;
    }

    /*GET VIDEO STATISTIC*/
    public function videosListById($part, $params)
    {

        $service = $this->youtube;
        $results = array();

        $params = array_filter($params);

        /*GET LIST*/
        $response = $service->videos->listVideos(
            $part,
            $params
        );

        foreach ($response['items'] as $item) {

            /*var_dump($item);
            exit();*/

            $video_detail = array(

                /*"likeCount" => $item["likeCount"],
                "viewCount" => $item["viewCount"]*/

                'id' => $item["id"],
                'statistics' => $item["statistics"],
                'contentDetails' => $item["contentDetails"]
            );

            $results[] = $video_detail;


        }

        return $results;
    }

    /*LIST YOUTUBE SEARCH*/
    public function search($pageToken = false)
    {

        try {

            // Call the search.list method to retrieve results matching the specified
            // query term.


/*
            $queryParams = [
                'maxResults' => 30,
                'q' => 'tay du ky',
                'type' => 'video'
            ];

            $response = $this->youtube->search->listSearch('snippet', $queryParams);*/


            $param = array(
                'q' => $this->keyword,
                'maxResults' => 20,
                'type' => 'video');

            //               'order' => 'viewCount',

//            var_dump($param);


            /*SET PAGE TOKEN*/
            if ($pageToken)
                $param['pageToken'] = $pageToken;

            /*GET SEARCH LIST DATA FROM YOUTUBE*/
            $searchResponse = $this->youtube->search->listSearch('id,snippet', $param);
//            var_dump($searchResponse);
//            exit();

            /*BEGIN INIT*/

            $videos = array();
            $list_id = array();

            // Add each result to the appropriate list, and then display the lists of
            // matching videos, channels, and playlists.
            foreach ($searchResponse['items'] as $searchResult) {

                /*GET VIDEO RESULT*/
                switch ($searchResult['id']['kind']) {
                    case 'youtube#video':

//                        var_dump($searchResult['snippet']['thumbnails']);

                        $item = array(

                            "title" => $searchResult['snippet']['title'],
                            "id" => $searchResult['id']['videoId'],
                            "thumbnail" => $searchResult['snippet']['thumbnails']['default']['url'] /*HIGH QUALITY*/
                        );

                        $list_id[] = $item["id"];

                        array_push($videos, $item);

                        break;
                    /*             case 'youtube#channel':
                                     $channels .= sprintf('<li>%s (%s)</li>',
                                         $searchResult['snippet']['title'], $searchResult['id']['channelId']);
                                     break;
                                 case 'youtube#playlist':
                                     $playlists .= sprintf('<li>%s (%s)</li>',
                                         $searchResult['snippet']['title'], $searchResult['id']['playlistId']);
                                     break;*/
                }
            }

            /*GET VIDEO STATISTIC AND VIEW COUNT*/
            //var_dump($list_id);

            $list_id = implode(",", $list_id);
            $details = $this->videosListById('contentDetails,statistics', array('id' => $list_id));

            /*MERGE ARRAY TO GET VIDEO DETAIL*/
            foreach ($details as $key => $item) {

//                var_dump($item);

                /*GET LIKE & DISLIKE STATISTIC*/
                if ($videos[$key]["id"] == $item["id"]) {

                    $statistic = $item["statistics"];
                    $detail = $item["contentDetails"];

                    $videoDetail = array(

                        /*RATING COUNT*/
                        "likeCount" => $statistic["likeCount"],
                        "dislikeCount" => $statistic["dislikeCount"],
                        "viewCount" => $statistic["viewCount"],

                        /*CONTENT DETAIL*/
                        "duration" => $detail["duration"],
                        "quality" => $detail["definition"]

                    );

                    $videos[$key] = array_merge($videoDetail, $videos[$key]);
                }
            }


            /*DATA*/
            $data = array(
                'lists' => $videos,

                'nextPage' => $searchResponse["nextPageToken"],
                'prevPage' => $searchResponse["prevPageToken"]
            );

//            var_dump($data);

            return $data;

        } catch (Google_Service_Exception $e) {
            echo $e->getMessage();


        } catch (Google_Exception $e) {
            echo $e->getMessage();

        }

        return false;
    }
}