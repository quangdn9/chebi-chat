<?php
/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 9/26/17
 * Time: 10:42 AM
 */
//namespace Kreait\Firebase\ServiceAccount;

if (!file_exists(__DIR__ . '/vendor/autoload.php')) {
    throw new Exception('please run "composer require google/apiclient:~2.0" in "' . __DIR__ . '"');
}

require_once __DIR__ . '/vendor/autoload.php';



use Firebase\Factory;
use Firebase\ServiceAccount;

$serviceAccount = ServiceAccount::discover();

$firebase = (new Factory)
    ->withServiceAccount($serviceAccount)
    ->create();
$database = $firebase->getDatabase();

$reference = $database->getReference('path/to/child/location');

/*METHOD 2 - CUSTOM*/
/*$serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/firebase_credentials.json');
$firebase = (new Factory)
    ->withServiceAccount($serviceAccount)
    ->create();*/

//sdsdas