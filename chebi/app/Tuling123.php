<?php
/*
 *    适用于tuling123 API V1.0和V2.0
 *
 *    version: 2.0
 *
 *    https://github.com/CorePlusPlus/tuling123
 *
 *    update: 2018-05-02
 */

//namespace faker\Tuling123;

class Tuling123
{

    private $apiKey;
    private $secret;
    private $text;
    private $userId = 1;
    private $selfInfo = '';
    private $url = 'http://openapi.tuling123.com/openapi/api/v2';

//    private $url = 'http://www.tuling123.com/openapi/api';

    public function __construct($apiKey, $secret, $userId, $selfInfo)
    {

        $this->apikey = $apiKey;
        $this->secret = $secret;
        $this->userId = ($userId);
        $this->selfInfo = $selfInfo;
        $this->timestamp = time();

    }

    public function setUserId($id)
    {
        $this->userId = md5($id);
        return true;
    }

    public function tuling($text, $raw = false)
    {

        $this->text = $text;
        $iv = '';
        $iv = str_repeat(chr(0), 16);

        $aesKey = md5($this->secret . $this->timestamp . $this->apikey);

        $param = [
            "reqType" => 0,
            'perception' => [
                'inputText' => [
                    'text' => $this->text,
                ],
                'selfInfo' => $this->selfInfo
            ],
            'userInfo' => [
                'apiKey' => $this->apikey,
                'userId' => $this->userId,
            ]
        ];

        /*$param = [

            'key' => $this->apikey,
            'info' => $this->text,
        ];*/

//        $cipher = base64_encode(openssl_encrypt(json_encode($param), 'aes-128-cbc', hash('MD5', $aesKey, true), OPENSSL_RAW_DATA, $iv));

        $postData = $param;

        /*$postData = [
            'key' => $this->apikey,
            'timestamp' => $this->timestamp,
            'data' => $cipher
        ];*/

//        $result = json_decode('['.$this->post($this->url,json_encode($postData)).']',true);

        $data = json_decode($this->post($this->url, json_encode($postData)));


        /*GET INTENT INFO*/
        $intent = $data->intent;
        $code = $intent->code;

//        var_dump($data->results);


        $result_type = $data->results[0]->resultType;

        /*CHECK IF URL TYPE*/
        if ($result_type == "url") {

            $data->results = array_reverse($data->results);
        }

        /*GET FIRST RESULT*/
        list($result) = $data->results;

        /*case*/
        /*switch ($result_type) {

            case "url":
                $value = $result->values->url;
                break;

            default:

                $value = $result->values->text;
                break;

        }*/

        $value = $result->values->text;

        $property = false;

        /*GET PROPERTY AS NEWS*/
        if (count($data->results) > 1) {

            list(, $property) = $data->results;


            $property = $this->property_init($property);

            //            var_dump($property);
        }


        $output = array(
            "value" => $value,
            "code" => $code,
            "type" => $result_type,
            "property" => $property
        );

//        var_dump($output);

        return $output;

//        return false;
    }

    private function property_init($data)
    {

//        $result = "";

        $type = $data->resultType;
        $result = $data->values;

//        var_dump($values);


        return $result;
    }

    private function post($url, $data)
    {

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 500);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_URL, $url);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;

    }

}

?>
