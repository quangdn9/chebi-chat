<?php
/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 9/11/17
 * Time: 5:18 PM
 */

/*TULING123 CHAT BOT */
require("app/TulingChat.php");
//require_once('api/simsimi_api_class.php');


/*RENDER FOR YOUTUBE*/
function youtube_init($youtubeData)
{


    /*GET NEXTPAGE URL*/
    $param = $_GET;
    $param["nextPage"] = $youtubeData["nextPage"];
    $url = http_build_query($param);

    /*PROPERTY FOR YOUTUBE*/
    $property = array(

        "type" => "youtube",
        "list" => $youtubeData["lists"],

//      "prevPage" => $youtubeData["prevPage"],

        "nextPage" => $youtubeData["nextPage"],
        "url" => $url
    );

    return $property;
}

/*WIKIPEDIA INFO*/
function wiki_init($data)
{

    /*PROPERTY FOR YOUTUBE*/
    $wiki_data = json_decode($data);

    $property = array(

        "type" => "wiki",
        "list" => $wiki_data

    );

    return $property;
}

/*GET SIMSIMI*/
function get_remote_chat($text, $lc = "vn")
{

    if (DEVMODE) {

        switch ($lc) {

            case "vn":
                $response = "đang thử nghiệm";
                break;
            case "en":
                $response = "i love you";
                break;
            default:
                $response = "我好爱你";
        }


    } else {

        /*TULING123 CHAT*/
        $Tuling = new TulingChat();

        $result = $Tuling->chat($text);
    }

    return $result;

    /*FORBIDDEN VIETNAMESE LANGUAGE FROM SIMSIMI*/
    /*  if ($lc == "vn" && $_GET["devmode"] != "simi") {

          $result = array(

              "response" => "Chebi chưa hiểu nội dung này, bạn có thể gợi ý trả lời cho Chebi không?",
              "msg" => "OK"
          );

//            OTHER LANGUAGES
      } else {*/


    /*FILTER BAD WORD*/
//        $ft = "0.0";
//        $normalProb = 5;
//        $result = $simsimi->simSimiConversation($lc, $ft, $normalProb, $text);


    /*FILTER*/
    /*if (isset($result["response"])) {

        $result["response"] = simsimi_filter($result["response"]);
    }*/

    /*SET-UP SOURCE METHOD*/
//    $result["source"] = "Chebi " . $lc;


}

/*FILTER KEYWORD FOR SIMSIMI RESPONSE*/
function simsimi_filter($text)
{

    $data = array(

        "simsimi" => "chebi",
        "sim" => "chebi"
    );

    foreach ($data as $source => $target) {

        $text = str_replace($source, $target, $text);
    }

    return $text;
}

//GET CHINESE APP
function get_local_data($param, $App)
{

    $property = "";
    /*CAN MAKE THE LIST RESPONSE | {response: [{},{}..array]} FOR APP, THE FIRST RESPONSE INTRODUCE & THE NEXT ONE IS RESULT*/

    $type = $param["app_type"];
    $keyword = $param["keyword"];

    /*GET DATA FROM LOCAL*/
    $result = $App->getData($type, $keyword);

//    echo count($result);

    if (count($result) == 0) {

        $response = "没有你想要的结果";
    }

    else {

        switch ($type) {

            case "story":

//            $App->setParam("Story");

                if (count($result) != 0) {

                    $response = "(" . $result->{"分类名称"} . ")".
                        "|". $result->{"标题"} .
                        "|". $result->{"内容"};

                }

                else {

                    $response = "没有你想要的故事";
                }


                break;

            case "foods":

//            $App->setParam("Food");
//            $result = $App->getData();

                /*GET PICTURE*/
                $property = $App->getImages($result->{"Title"});
                $response = $result->{"Cat"} . "." . $result->{"Title"};

                break;

            case "idiom":

//                var_dump($result);
//                echo $result->{"cyname"};

//                exit();

                $App->setParam("Idiom");


                /*GET PICTURE*/
                    $property = $App->getImages($result->{"cyname"});

                $response = "成语" . $result->{"cyname"} .
                    "。<br/>解释: " . $result->{"explain"}.
                    "<br/>来源: ". $result->{"cyfrom"};

                break;

            default:

                break;
        }
    }


    /*OUTPUT*/
    $result = array(

        "response" => $response,
        "msg" => "OK",
        "source" => "App: tieng trung"
    );

    if ($property != "") {
        $result["property"] = $property;
    }


//    var_dump($result);
//    exit();

    return $result;

}

/*DETECT LANGUAGE*/
function detect_lang($text)
{


    /*CHECK LENGTH*/
    if (strpos($text, " ")) {

        $text_list = explode(" ", $text);
        $text = $text_list[0] . " " . $text_list[1];
    }

    $translator = new Translator($text);
    $translator->setApikey(GOOGLE_API_KEY);

    /*DETECT LANGUAGE*/
    $lang = $translator->detechLang();

    /*CHECK WITH LANG MAP*/
    $lang = $translator->checklangmap($lang->language, true);

    return $lang;
}

