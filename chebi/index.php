<!DOCTYPE html>
<html>
<head>

<!--    <link rel="shortcut icon" type="image/x-icon" href="/assets/favicon.ico"/>-->

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <title>Chebi Chat</title>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/> <!--320-->
<!--    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">-->
    <meta charset="utf-8">

    <!--HEADER-->
    <?php require("./tmpl/header.php"); ?>


</head>

<body>


<!--HEADER-->
<div class="titlebar" style="">
    <div class="title-container center">

        <img src="/assets/chebichat_design/logo.png" id="chebi-logo" height="60"/>
        <div class="config-icon">

            <a class="modal-trigger" href="#setting_modal">
<!--                <i class="material-icons">menu</i></a>-->
                <img src="/assets/chebichat_design/options_icon.png" width="50"/>
            </a>
        </div>
    </div>


</div>


<!--MENU-->

<!--CHAT BODY-->
<div id="chat_body" class="chatroom">
    <br><br><br>

    <div class="chat_simi dummy">

        <img src="/assets/chebichat_design/avatar_icon_2.png" class="circle large chebi_avatar">

		<span class="bubble">
			<span class="typing_ani"></span>
		</span>
    </div>
    <div class="chat_user dummy">
        <img src="" alt="" class="circle large user_avatar" style="display: none;">

        <span class="bubble"> . . . </span>
    </div>


    <br id="bottom_margin"><br>
</div>


<!--INPUT AREA-->
<div class="row col s10 chat_input white" style="margin-bottom: 0px; left: 0px; ">

    <div class="input-field col s1" style="margin:0px; padding:0px; ">


       <!-- <a id="record" class="" style="float: left;" href="javascript:void();">
            <img src="/assets/chebichat_design/lotus_button.png" width="50" />

        </a>-->

        <a id="record" class="btn-floating btn-medium">
            <i class="large material-icons">mic_off</i>
        </a>

    </div>

    <div class="input-field col s10" style="margin:0px;">

        <input placeholder="Type your message" id="reqText" type="text"
               style="background-color:white;border-bottom:1px solid #ccc;padding:0px 0px" class="validate"
               onkeydown="onEnterKeyDown();">

    </div>

    <div class="input-field col s1" style="margin:0px; padding:0px; ">

        <a id="recordbtn" class="" style="float: right;" href="javascript:void();">
        <img src="/assets/chebichat_design/send_icon.png" width="40" />

        </a>

    </div>
</div>

<div class="fixed-action-btn" style="bottom: 180px; ">
    <a id="youtube_btn" onclick="Youtube.openYoutubeModal();" class="btn-floating btn-large orange" style="display: none;">
        <i class="large material-icons">ondemand_video</i>
    </a>

</div>

<!--INPUT BUTTON-->
<!--<div class="fixed-action-btn" style="bottom: 100px;">

</div>-->


<?php
require("./tmpl/component.php");
?>

<!--MODALS-->
<?php
require("./tmpl/modals.php");
?>

</body>
</html>

