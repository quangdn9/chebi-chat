<?php
/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 11/5/17
 * Time: 4:31 PM
 */

//header("Content-type: application/json; charset:utf-8");

define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
require(ROOT . "/db_conn.php");

require("baidu/review.php");
require ("translator.php");

/*
 * type参数	说明	实例
1	酒店	『酒店设备齐全、干净卫生』->『酒店设备齐全』、『干净卫生』
2	KTV	『环境一般般把，音响设备也一般，隔音太差』->『环境一般』、『音响设备一般』、『隔音差』
3	丽人	『手法专业，重要的是效果很棒』->『手法专业』、『效果不错』
4	美食餐饮	『但是味道太好啦，舍不得剩下』->『味道不错』
5	旅游	『景区交通方便，是不错的旅游景点』->『交通方便』、『旅游景点不错』
6	健康	『环境很棒，技师服务热情』->『环境不错』、『服务热情』
7	教育	『教学质量不错，老师很有经验』->『教学质量不错』、『老师有经验』
8	商业	『该公司服务好，收费低，效率高』->『服务好』、『收费低』、『效率高』
9	房产	『该房周围设施齐全、出行十分方便』->『设施齐全』、『出行方便』
10	汽车	『路宝的优点就是安全性能高、空间大』->『安全性能高』、『空间大』
11	生活	『速度挺快、服务态度也不错』->『速度快』、『服务好』
12	购物	『他家的东西还是挺贵的』->『消费贵』
13	3C	『手机待机时间长』->『待机时间长』*/
$type = false;

if (isset($_GET["type"])) {

    $type = $_GET["type"];

}

/*ACTION*/
$act = "default";
if (isset($_GET["act"])) {

    $act = $_GET["act"];
}

/*LANGUAGE*/
$lang = "zh";

if (isset($_GET["lang"])) {

    $lang = $_GET["lang"];
}

/*IF LANG IS VIETNAMESE -> NEED TO TRANSLATE FIRST.*/

// 定义参数变量
$keyword = '味道很不错，很喜欢吃。服务也很好感觉很亲切，吃的很舒服，谢谢';




if (isset($_GET["keyword"])) {

    $keyword = $_GET["keyword"];

    /*translate all other languages to Chinese :)*/
    if ($lang != "zh") {

        /*translate*/
        $translator = new Translator($keyword);
        $keyword = $translator->translate("zh");

    }

//    var_dump($keyword);
//    exit();


    $result = "";

    /*SETUP MACHINE*/
    $aipNlp = new AipNlp($baidu_conf["AppID"], $baidu_conf["Apikey"], $baidu_conf["Secret"]);

    $Review = new Review($aipNlp, $keyword);


    $sentiment = "";

//    echo $act;

    switch ($act) {

        case "sentiment":

            if (DEVMODE) {

                require("sample/sentiment.json");
                return;
            }

            $sentiment = $Review->getSentiment();
            $sentiment = $sentiment["items"][0];


            $result = array(

                "sentiment" => $sentiment
            );

            break;

        case "review":

            if (DEVMODE) {

                require("sample/review.json");
                return;
            }

            $review_tags = $Review->getReviewTags($type);

            $result = array(

                "review" => $review_tags

            );

            break;

        default:

            /*SENTIMENT*/
            $sentiment = $Review->getSentiment();
            $sentiment = $sentiment["items"][0];

            /*----------*/

            /*REVIEW*/
            $review_tags = $Review->getReviewTags($type);

//            echo $keyword."<br/>";

            /*GET REVIEW TAGS*/
            if (isset($review_tags["items"])) {

                $review_tags = $review_tags["items"];

                /*check language*/
                if ($lang != "zh") {

                    $Review->setTranslator($translator);

                    $review_tags = $Review->translateReview($review_tags, $lang);
                }
            }

            /*NO HAVE REVIEW TAGS*/
            else {

                $review_tags = "";
            }

            $result = array(

                "review" => $review_tags,
                "sentiment" => $sentiment
            );

            break;

    }

    $result["keyword"] = $keyword;

//    var_dump($result);

    echo json_encode($result);

}
