<?php

/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 10/3/17
 * Time: 4:15 PM
 */
//require("AipHttpClient.php");

class Translator
{
    private $keyword;
    private $apikey;

    /**
     * @param mixed $apikey
     */
    public function setApikey($apikey)
    {
        $this->apikey = $apikey;
    }

    /**
     * @param mixed $keyword
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;
    }

    /**
     * @return mixed
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * @param mixed $keyword
     */

//    private $baseURL = "https://google-translate-proxy.herokuapp.com/api/translate";
    private $baseURL = "https://translate.chebichat.com/api/translate";
    private $http;

    public function __construct($keyword, $http = false)
    {

        $this->keyword = $keyword;

        if ($http) {
            $this->http = $http;
        } else {

            $this->http = new AipHttpClient();
        }


    }


    public function detechLang()
    {

        $url = "https://translation.googleapis.com/language/translate/v2/detect";

        $param = array(

            "key" => $this->apikey,
            "q" => $this->keyword
        );


        $result = $this->http->get($url, $param);
        $data = json_decode($result["content"]);


        return $data->data->detections[0][0];
    }

    public function translate($target = "zh-CN")
    {

        /*https://google-translate-proxy.herokuapp.com/api/translate
        ?query=qu%E1%BA%A7n%20%C3%A1o&targetLang=zh&sourceLang=auto*/

        $url = $this->baseURL;

        /*MAP LANG*/
        $target = $this->checklangmap($target);

        $param = array(

            "query" => $this->keyword,
            "targetLang" => $target,
            "sourceLang" => "auto"

        );

        $result = $this->http->get($url, $param);
        $data = json_decode($result["content"]);

        $data = $data->extract->translation;

        return $data;
    }

    public function checklangmap($lang, $reverse = false)
    {

        $map = array(

            "vn" => "vi",
            "zh" => "zh-CN",
            "ch" => "zh-TW"
        );

        foreach ($map as $key => $value) {

            /*GOOGLE LANG -> CHEBI LANG*/
            if ($reverse && $lang == $value) {

                $lang = $key;
            } /*CHEBI LANG -> GOOGLE LANG*/
            else if ($lang == $key) {

                $lang = $value;
            }
        }

        return $lang;
    }

    public function multiTranslate($target = array())
    {

        $data = array();

        foreach ($target as $lang) {

            /*$item = array(
                "lang" => $lang,
                "result" => $this->translate($lang)
            );

            $data[] = $item;*/

            $data[$lang] = $this->translate($lang);
        }


        return $data;
    }


}