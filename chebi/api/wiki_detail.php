<?php
/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 9/13/17
 * Time: 2:07 PM
 */
header("Content-type: text/html; charset:utf-8");
//header("Content-type: text/json; charset:utf-8");

define("ROOT", $_SERVER["DOCUMENT_ROOT"]);

require(ROOT."/db_conn.php");
require(ROOT."/includes/hecheng_baidu/AipSpeech.php");

/*WIKI CLASS*/
require ("wiki_api.php");

$lc = "vi";

if  (isset($_GET["lc"])) {

    $lc = $_GET["lc"];
}

/*BEGIN*/
if (isset($_GET["keyword"])) {

    /*DEV*/
    if (DEVMODE) {

        $data = file_get_contents("wiki_detail.json");
    }
    else {

        $fullURL = $_GET["keyword"];

        /*https://vi.wikipedia.org/wiki/M%E1%BB%B9_T%C3%A2m*/

        $wiki = new Wiki($fullURL, $lc);
        $data = $wiki->getDetail();

    }


    /*if (isset($wiki_data["error"])) {

        $wiki_data = $wiki_data["error"]["info"];
    }*/

    echo $data;

//    var_dump($data);

    //echo $data; /*JSON*/

}

