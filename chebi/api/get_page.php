<?php
/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 9/28/17
 * Time: 1:57 PM
 */

header("Content-type: application/json; charset:utf-8");
define("ROOT", $_SERVER["DOCUMENT_ROOT"]);

require("backend.php");
require(ROOT . "/db_conn.php");


if (isset($_GET["pageid"])) {

    $pageid = $_GET["pageid"];


    if (DEVMODE) {

//        $url = "http://localhost:3000/pages/pageid";

        echo file_get_contents("sample/getpage.json");

        return;

    } else {

        $url = "https://backend.chebichat.com/pages/pageid";
    }

    /*REMOVE WHEN UPLOAD*/
//    $url = "http://localhost:3000/pages/pageid";


    $app = new Backend($url);
    $result = $app->getPages($pageid);

    if ($result == "error pageid") {

        echo "null";

    } else {

        echo $result;
    }

//    var_dump($result);

}