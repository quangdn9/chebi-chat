<?php

/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 11/24/17
 * Time: 5:25 PM
 */
class Chebi_training
{
    private $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;

        return;
    }

    public function add_local_db($sql)
    {

        $result = $this->conn->query($sql);
        //echo "Total: ".$result->num_rows."<br/>";

       $id = $this->conn->insert_id;

        if ($id != 0) {

            return [

                "result" => "successful",
                "id" => $id
            ];
        }

//        $query = $result->affected_rows;
//        var_dump($result);

        return false;

    }
}