<?php

/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 11/9/17
 * Time: 12:55 AM
 */
class Sensitive
{
    private $keyword;
    private $translator;
    private $db;

    private $http;

    /**
     * @return mixed
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * @param mixed $keyword
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;
    }


    public function __construct($keyword, $db)
    {

        /*CHANGE TO NORMAL KEYWORD*/
        $keyword = mb_strtolower($keyword);

        /*DEFINE PARAM*/
        $this->keyword = $keyword;
        $this->db = $db;

        /*define object*/
        $this->translator = new Translator($keyword);
        $this->http = new AipHttpClient();

        return;
    }

    /*FILTER MAIN FUNC*/
    public function filter($lang = "vn")
    {

        $keyword = $this->keyword;

        /*CHECK FAMILY NAME*/
        $result_name = $this->check_people_name($keyword);

        if ($result_name) {

            $result_name->{"source"} = "sensitive filter by people name";

            /*var_dump($result_name);
            exit();*/

            return $result_name;
        }

        /*------------*/

        /*CHECK IN Database without translate first (search with vietnamese keyword)*/
        $result_keyword = $this->check_keyword_sensitive($keyword, $lang);

       /* var_dump($result_keyword);
        exit();*/

        /*COMMON FOR ALL OF THE LANGUAGE, SEARCH IN LOCAL SENSITIVE DATABASE FIRST*/
        if ($result_keyword) {

            $result_keyword->{"source"} = "sensitive filter in local db";

            return $result_keyword;
        }

        /*-----------*/

        /*CHECK IN WEB-PURIFY (except vn)*/
        if ($lang == "zh" || $lang == "en") {

            $result_pure = $this->check_web_pure($keyword, $lang);

            /*COMMON FOR ALL OF THE LANGUAGE, SEARCH IN LOCAL SENSITIVE DATABASE FIRST*/
            if ($result_pure) {

                $result_pure->{"source"} = "sensitive filter with webpurify";

                return $result_pure;
            }
        }

        return false;

        /*------->*/
        /*dont use now*/



    }

    /*CHECK IN WEB-PURIFY*/
    public function check_web_pure($keyword, $lang = "en")
    {

        $url = "http://api1.webpurify.com/services/rest/";
        $apikey = "b5086e657bb59f3c0664681c5cf61397";

        $param = array(

            "method" => "webpurify.live.check",
            "api_key" => $apikey,
            "format" => "json",
            "lang" => "$lang",
            "text" => $keyword

        );

        $result = $this->http->get($url, $param);

        $data = json_decode($result["content"]);

        /*FLAG 1 IS SENSITIVE, 0 IS NORMAL*/
        $flag = ($data->rsp->found);

        if ($flag == 0) {

            return false;
        }


        return $data;
    }

    /*CHECK BY KEYWORD IN SENSITIVE TABLE*/
    public function check_keyword_sensitive($keyword, $lang = "vn")
    {

//        var_dump($lang);

        if ($lang == "vn") {

            $sql = "SELECT * FROM `sensitive` WHERE

                '" . $keyword . "' LIKE BINARY CONCAT('%',keyword,'%') AND

                `cat` NOT LIKE '%姓名%'

                LIMIT 0, 1000";

            /*ALSO SEARCH PEOPLE NAME*/

        } else {

            $sql = "SELECT * FROM `sensitive` WHERE

                '" . $keyword . "' LIKE CONCAT('%',keyword,'%') AND

                CHARACTER_LENGTH(`keyword`) > 1 AND `lang` LIKE '".$lang."'

                LIMIT 0, 1000";
        }

        /*`keyword` LIKE '%" . $keyword . "%'*/


//        echo $sql;

        /*query*/
        $result = $this->query($sql);

        return $result;
    }

    /*CHECK people name CHAT LOCAL*/
    public function check_people_name($keyword, $type = "family")
    {

        /*search by family name*/
        if ($type == "family") {

            $sql = "SELECT * FROM `sensitive` WHERE

                '" . $keyword . "' LIKE BINARY CONCAT(keyword,' %')
                AND `cat` LIKE '姓名'

                LIMIT 0, 1000";

        }

        $result = $this->query($sql);

        return $result;
    }


    /*CHECK 骂人词汇 IN CHAT LOCAL*/
    public function check_chat_local($keyword)
    {

        $sql = "SELECT * FROM `thuvien_tiengtrung`.`chat_local` WHERE

              `分类` LIKE '%骂人词库%' AND

              '" . $keyword . "' LIKE CONCAT('%',输入,'%') OR
              `输入` LIKE '%" . $keyword . "%' OR

              LIMIT 0, 1000";

        /* `机器` LIKE '%" . $keyword . "%'*/


        $result = $this->query($sql);

        return $result;
    }

    /*CHECK IN DATABASE*/
    public function query($sql)
    {

        /*SELECT FROM DB*/
        echo $sql;

        $result = $this->db->query($sql);

        var_dump($result);


        if ($result->num_rows > 0) {

            $data = $result->fetch_object();

            return $data;

        }


        return false;
    }

}