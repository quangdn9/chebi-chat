<?php

/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 9/12/17
 * Time: 11:18 AM
 */
class Wiki
{
    private $keyword;
    private $url;
//    private $youtube;

    public function __construct($keyword, $lang = "vi")
    {
        /*if ($url == "") {

            $url = "https://vi.wikipedia.org/w/api.php";
        }*/

        $this->makeURL($lang);
        $this->keyword = $keyword;
    }

    /*MAKE URL FOR WIKI*/
    public function makeURL ($lang) {

        $langmap = array(

            "vn" => "vi",
            "hk" => "zh"

        );

        if (isset($langmap[$lang])) {

            $lang = $langmap[$lang];
        }

        $url = "https://".$lang.".wikipedia.org/w/api.php";

        $this->url = $url;
//        exit();

        return $url;
    }

    /*SEARCH BY KEYWORD*/
    public function search() {


        $param = array(

            "action" => "opensearch",
            "search" => $this->keyword,
            "format" => "json",
            "redirects" => "resolve"
        );

//        echo $this->keyword;
//        var_dump($param);
//        echo $this->url;

        $http = new AipHttpClient();


//        exit();

        $result = $http->get($this->url, $param);

        return $result;
    }


    /*GET DETAIL FROM SEARCH*/
    public function getDetail() {

        $fullURL = $this->keyword;

        /*https://vi.wikipedia.org/wiki/M%E1%BB%B9_T%C3%A2m*/

//        var_dump($fullURL);

//        exit();

        if (strpos($fullURL,"/wiki/")) {

            list ($url, $keyword) = explode("/wiki/", $fullURL);
            $url .= "/w/api.php";
        }

        else if (strpos($fullURL,"index.php?title=")) {

            list ($url, $keyword) = explode("index.php?title=", $fullURL);
            $url .= "api.php";

//            Chim_sáo_ngày_xưa&action=edit&redlink=1

            if (strpos($keyword,"&")) {

                list($keyword) = explode("&", $keyword);
            }
        }

        else {

            return false;
        }


        $this->url = $url;

        $param = array(

            "action" => "parse",
            "page" => urldecode($keyword),
            "format" => "json",
            "mobileformat" => "true"
        );


       /* echo $this->url;
        var_dump($param);
        exit();*/


        $http = new AipHttpClient();
        $result = $http->get($this->url, $param);

//        var_dump($result);

        if ($result["code"] == 200) {

            $content = $result["content"];

            return $content;
        }


        return false;
    }
}