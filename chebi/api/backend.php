<?php
/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 9/28/17
 * Time: 1:44 PM
 */

require("AipHttpClient.php");

class Backend
{

    private $uid;

    private $http;
    private $backend_URL;

    public function __construct($url)
    {
        $this->http = new AipHttpClient();
        $this->backend_URL = $url;

        return;

    }

    /*GET USER*/
    function getUsers($uid)
    {

        $param = array(
            "uid" => $uid
        );

        $url = $this->backend_URL . "/" . $uid;

        $result = $this->http->get($url, $param);

        //echo $url.

        return $result["content"];
    }

    /*GET PAGE INFORMATION*/
    function getPages($pageid)
    {

        $url = $this->backend_URL . "/" . $pageid;

//        exit();

        $result = $this->http->get($url);

        return $result["content"];
    }

    function saveUsers($uid, $data)
    {


        $url = $this->backend_URL . "/" . $uid;


        $param = array(

            "data" => $data
        );

        $result = $this->http->post($url, $param);


        return $result["content"];
    }
}

