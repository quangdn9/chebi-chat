<?php
/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 9/28/17
 * Time: 1:57 PM
 */

require("backend.php");
define("ROOT", $_SERVER["DOCUMENT_ROOT"]);

require(ROOT . "/db_conn.php");

//header("Content-type: text/json; charset:utf-8");

$uid = "";
$url = "https://backend.chebichat.com/users/uid";

//echo file_get_contents("sample/getuser.json");
//return;

if (isset($_GET["uid"])) {

    $uid = $_GET["uid"];

    $app = new Backend($url);
    $result = $app->getUsers($uid);

    if ($result == "error uid") {

        echo "null";
    } else {

        echo $result;
    }

//    var_dump($result);

}