<?php
/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 9/28/17
 * Time: 1:57 PM
 */

require("backend.php");
define("ROOT", $_SERVER["DOCUMENT_ROOT"]);

require(ROOT . "/db_conn.php");

//header("Content-type: text/json; charset:utf-8");

$uid = "";
$url = "https://backend.chebichat.com/users/save";

if (DEVMODE) {
    $url = "http://localhost:3000/users/save";
}

if (isset($_POST["uid"])) {

    $uid = $_POST["uid"];
    $data = $_POST["data"];

    if (DEVMODE) {

        echo "saved";
    } else {


        $app = new Backend($url);
        $result = $app->saveUsers($uid, $data);

        echo $result;
    }
}