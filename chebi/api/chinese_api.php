<?php

/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 10/3/17
 * Time: 3:43 PM
 */

//use Sunra\PhpSimple\HtmlDomParser;
include("lib/simple_html_dom.php");

class Chinese_api
{
    private $kwd;
    private $stroke_url = "https://hanzi.chebichat.com/api/hanzi-writer/"; //https://www.chineseconverter.com/en/convert/chinese-stroke-order-tool

    private $http;

    public function __construct($kwd)
    {

        $this->kwd = $kwd;
        $this->http = new AipHttpClient();

        return;
    }


    public function getStroke() {

//        echo $this->kwd;

        $data = ($this->mb_str_split($this->kwd));

        foreach ($data as $key=>$item) {

            $data[$key] = $this->stroke_url.$item.".gif";
        }

//        var_dump($data);

//        exit();

        return $data;
    }

    /**
     * Convert a string to an array
     * @param string $str
     * @param number $split_length
     * @return multitype:string
     */
    public function mb_str_split($str,$split_length=1,$charset="UTF-8"){
        if(func_num_args()==1){
            return preg_split('/(?<!^)(?!$)/u', $str);
        }
        if($split_length<1)return false;
        $len = mb_strlen($str, $charset);
        $arr = array();
        for($i=0;$i<$len;$i+=$split_length){
            $s = mb_substr($str, $i, $split_length, $charset);
            $arr[] = $s;
        }
        return $arr;
    }

    /*public function getStrokeChineseConvert()
    {

        $output = array();

        preg_match_all('/[\x{4e00}-\x{9fa5}]/u',$this->kwd,$result);

        list($result) = $result;

        foreach ($result as $item) {

            $output[] = "http://chebichat.com:8900/api/hanzi-writer/".$item.".gif";
        }

        return $output;
    }

    /*public function getStroke_bk()
    {


        if (DEVMODE) {


        }

        $param = array(

            "text" => $this->kwd,
            "stroke_color" => "000000",
            "transient_color" => "FF0000",
            "sequence_background_color" => "FFFFFF",
            "type" => "0",
            "type_value" => "0",
            "speed" => "normal"
        );

        $data = $this->http->post($this->stroke_url, $param);
        $html = $data["content"];

        echo $html;

        exit();

        list ($first, $last) = explode('id="img_container"', $html);
        list ($last) = explode('</div>', $last);

        list (, $html) = explode("\n", $last);


//        exit();

//        $html = HtmlDomParser::str_get_html($data["content"]);
        $html = str_get_html($html);



        $stroke = $html->find(".stroke_order");

//        var_dump($stroke);

//        exit();
        $result = array();


        foreach ($stroke as $item) {


            $img = "https://www.chineseconverter.com" . $item->{"data-original"};
            $result[] = $img;
        }


        return $result;
    }*/


}

