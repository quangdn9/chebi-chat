<?php

/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 10/3/17
 * Time: 3:43 PM
 */


class Taobao_api
{
    private $kwd;

    public function __construct($kwd)
    {

        $this->kwd = $kwd;

        return;
    }

    public function getKeyword($lang = "zh") {

        $translator = new Translator($this->kwd);
//        $keyword = $translator->multiTranslate($lang);


        if (DEVMODE) {

            return [

                array(
                    "lang" => "zh",
                    "result" => "手表"
                ),

                array(
                    "lang" => "en",
                    "result" => "watch"
                )
            ];
        }


        $keyword = $translator->multiTranslate($lang);

        return $keyword;
    }


}

