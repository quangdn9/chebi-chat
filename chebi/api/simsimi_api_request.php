<?php
/**
 * Simsimi API Class
 *
 * @author     M Teguh A Suandi
 * @link       http://mtasuandi.wordpress.com
 * @license    http://creativecommons.org/licenses/by/3.0/
 *
 */
function simsimi_api_request($api_key, $params)
{

    $method = "GET";
    $host = "http://api.simsimi.com/request.p";

    //$host = "http://sandbox.api.simsimi.com/request.p"; //trail
    //$params["fd76a969-c7ed-456c-8227-bd45bb43cb4f"]   	= $api_key;

    ksort($params);

    $canonicalized_query = array();

    foreach ($params as $param => $value) {
        $param = str_replace("%7E", "~", rawurlencode($param));
        $value = str_replace("%7E", "~", rawurlencode($value));
        $canonicalized_query[] = $param . "=" . $value;
    }

//    var_dump($canonicalized_query);

    $canonicalized_query = implode("&", $canonicalized_query);

    $request = $host . "?key=" . $api_key . "&" . $canonicalized_query;

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $request);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 15);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

    $json_response = curl_exec($ch);
    curl_close($ch);


//    echo $json_response;

    $result = json_decode($json_response, true);


    if ($json_response === false) {
        return False;
    } else {
        return $result;
    }
}

?>
