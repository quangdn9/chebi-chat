<?php

/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 11/5/17
 * Time: 3:40 PM
 */

//echo ROOT;
//require(ROOT."/includes/api_config.php");

require 'aip-php-sdk-1.6.7/AipNlp.php';


class Review
{

    private $keyword;
    private $aipNlp;
    private $translator;

    /**
     * @param mixed $translator
     */
    public function setTranslator($translator)
    {
        $this->translator = $translator;
    }


    public function __construct($aipNlp, $keyword)
    {
//        $this->keyword = $keyword;

        $this->keyword = $keyword;
        $this->aipNlp = $aipNlp;

        return;
    }

    public function getReviewTags($type = false)
    {

        $keyword = $this->keyword;

//        var_dump($type);

        // 调用评论观点抽取接口
        if ($type) {

            $option = array('type' => $type); // 汽车分类
            var_dump($option);

            $result = $this->aipNlp->commentTag($keyword, $option); // 默认为美食分类

        } // 调用情感观点抽取接口
        else
            $result = $this->aipNlp->commentTag($keyword); // 默认为美食分类


        return $result;
    }

    /*情感倾向分析*/
    public function getSentiment($option = false)
    {

        $result = $this->aipNlp->sentimentClassify($this->keyword);

        return $result;

    }

    /*translate review tags*/
    public function translateReview($review_tags, $lang)
    {

        $translator = $this->translator;

        /*LIST OF ADJ*/
        $adj_list = array();
        $prop_list = array();

        /*LIST FROM TAGS REVIEW*/
        foreach ($review_tags as $item) {

            $adj_list[] = trim($item["adj"]);
            $prop_list[] = trim($item["prop"]);
        }

        /*TRANSLATE*/
        $text = implode(",", $prop_list) . "-" . implode(",", $adj_list);

//        echo $text."<br/>";
//exit();

        /*ADD NEW TRANSLATE TEXT*/
        $translator->setKeyword($text);

        /*TRANSLATE TAGS*/
        $text = $translator->translate($lang);

        /*-------->*/
        /*output*/

        //        echo $text."<br/>";

        /*HAVE TRANSLATE RESULT*/
        if (strpos($text, "-")) {

            /*output result*/
            list ($prop_list, $adj_list) = explode("-", $text);

            $prop_list = explode(",", $prop_list);
            $adj_list = explode(",", $adj_list);

            /*LIST FROM TAGS REVIEW -> UPDATE $review_tags*/
            foreach ($review_tags as $key => $item) {

                $review_tags[$key]["adj"] = trim($adj_list[$key]);
                $review_tags[$key]["prop"] = trim($prop_list[$key]) . " ";

            }
        }

        else {

            $review_tags[0]["prop"] = $text;
        }

//exit();

//var_dump($review_tags);

        return $review_tags;
    }

}





