<?php
/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 8/2/17
 * Time: 5:24 PM
 */


	class Tiengtrung
    {
/*        private $imageTitle;
        private $imageCat;
*/
        private $conn;

        function __construct($conn, $title)
        {
            # code...
            $this->title = $title;
            $this->conn = $conn;
            //$this->cat = $cat;
        }

        public function getTourDetail() {

            $sql = "SELECT * FROM `thuvien_tiengtrung`.`TourScene` WHERE `景点` LIKE '%".$this->title."%' LIMIT 0, 1000";

            $result = $this->conn->query($sql);
            //echo "Total: ".$result->num_rows."<br/>";

            $row = "";

            if ($result->num_rows > 0) {

                $row = $result->fetch_object();
            }
            $row->{"内容"} = nl2br($row->{"内容"});


            return $row;
        }

        public function getFoodDetail() {

            $sql = "SELECT * FROM `thuvien_tiengtrung`.`FoodAlbum` WHERE `Title` LIKE '%".$this->title."%' LIMIT 0, 1000";

            $result = $this->conn->query($sql);
            //echo "Total: ".$result->num_rows."<br/>";

            $row = "";

            if ($result->num_rows > 0) {

                $row = $result->fetch_object();
            }

            return $row;
        }

        /*TEXT2VOICE*/
        public function getIdiomDetail() {

            $sql = "SELECT * FROM `thuvien_tiengtrung`.`IdiomExplain` WHERE `cyname` LIKE '%".$this->title."%' LIMIT 0, 1000";

            $result = $this->conn->query($sql);
            //echo "Total: ".$result->num_rows."<br/>";

            $row = "";

            if ($result->num_rows > 0) {

                $row = $result->fetch_object();
            }

            return $row;
        }
    }
?>