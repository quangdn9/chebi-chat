<?php

/**
 * Created by PhpStorm.
 * User: donhatquang
 * Date: 8/14/17
 * Time: 2:41 PM
 */

require("AipHttpClient.php");

class getSimsimi
{

    private $header;
    private $param;
    private $http;
    private $host = "http://simsimi.com/getRealtimeReq";


    public function __construct($cookie = array(), $param = array())
    {
        $headertext = "Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.8,de;q=0.6,zh-CN;q=0.4,zh;q=0.2,vi;q=0.2,zh-TW;q=0.2,en-GB;q=0.2,km;q=0.2
Content-Type: application/json;charset=utf-8
Referer: simsimi.com/";

        if ($cookie != "") {
            //$this->header = array_merge($cookie, $this->setHeader($headertext));
            $this->header = $cookie + $this->setHeader($headertext);

//            var_dump($cookie);

        } else {

            $this->header = $this->setHeader($headertext);
        }

        /*PARAM*/
        $url = "http://simsimi.com/getRealtimeReq?lc=zh&ft=1&normalProb=0&reqText=đồ điên&status=W&talkCnt=11";

        if ($param != "") {
//            $this->param = array_merge($param, $this->setParam($url));

            $this->param = $param + $this->setParam($url);
        }


        /*HEADER*/
        $this->http = new AipHttpClient($this->header);

        return;
    }

    /*SET HEADER FROM TEXT*/
    public function setHeader($headertext)
    {

        $headerlist = explode("\n", $headertext);

        $header = array();

        foreach ($headerlist as $item) {

            list ($k, $v) = explode(":", $item);
            $header[$k] = trim($v);

        }
        return $header;
    }

    /*SET PARAM FROM TEXT*/
    public function setParam($url)
    {

        $remove_http = str_replace('http://', '', $url);
        $split_url = explode('?', $remove_http);
        $get_page_name = explode('/', $split_url[0]);
        $page_name = $get_page_name[1];

        $split_parameters = explode('&', $split_url[1]);


        for ($i = 0; $i < count($split_parameters); $i++) {
            $final_split = explode('=', $split_parameters[$i]);
            $split_complete[$page_name][$final_split[0]] = $final_split[1];
        }

        return $params = ($split_complete["getRealtimeReq"]);
    }

    /*GET DATA*/
    public function getChat($chat)
    {

        $this->param["reqText"] = $chat;

        /*var_dump($this->header);
        var_dump($this->param);*/
        //return;

        return $data = $this->http->get($this->host, $this->param);
    }
}