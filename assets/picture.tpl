<!-- TIENG TRUNG -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js"></script>
<script type="text/javascript">

    var Image = {

        title: "{$current.TITLE_ESC}",
        alt: "{$ALT_IMG}"

    };
    var devmode = false;
</script>

{*script*}
{*{combine_script id='mustache' load='async' path='Tiengtrung/assets/js/node_modules/mustache/mustache.min.js'}*}

{*LIB API*}
{combine_script id='baidu_api' load='async' require='jquery' path='tiengtrung/assets/js/baidu_api.js'}

{*MAIN*}
{combine_script id='tiengtrung' load='async' require='jquery' path='tiengtrung/assets/js/tiengtrung.js'}
{combine_script id='tiengtrung_main' load='async' require='jquery' path='tiengtrung/assets/js/picture_main.js'}

{*CSS*}
{combine_css id='Tiengtrung' path='tiengtrung/assets/Tiengtrung.css'}

{*BUTTON*}
<div class="btn-group" data-toggle="buttons" id="baidu_voice_btn">
    <label class="btn btn-warning active" style="color:#000;">
        <input type="radio" name="options" id="option1" data-code="0" autocomplete="off" checked>Giong nữ
    </label>
    <label class="btn btn-warning"  style="color:#000;">
        <input type="radio" name="options" id="option2" data-code="1" autocomplete="off">Giọng nam
    </label>
    <label class="btn btn-warning"  style="color:#000;">
        <input type="radio" name="options" id="option3" data-code="3" autocomplete="off">Truyền cảm nam
    </label>
    <label class="btn btn-warning"  style="color:#000;">
        <input type="radio" name="options" id="option4" data-code="4" autocomplete="off">Nhí nhảnh
    </label>

{*VOLUME*}
    <div class="form-group" id="speed">
        <label class="control-label col-md-5"><strong>Tốc độ đọc </strong></label>
        <div class="input-group number-spinner col-md-7">
                        <span class="input-group-btn">
                            <a class="btn btn-danger" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></a>
                        </span>
            <input type="text" disabled name="adult" id="speed_val" class="form-control text-center" value="5" max=9 min=1>
                        <span class="input-group-btn">
                            <a class="btn btn-info" data-dir="up"><span class="glyphicon glyphicon-plus"></span></a>
                        </span>
        </div>
    </div>

</div>
