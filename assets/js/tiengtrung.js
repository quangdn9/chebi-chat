var TiengTrung = {

    imageTitle: "",
    imageCategories: "",
    imageData: {},
    voiceSource: "",
    textMaximum: 300,

    basehost: "/tiengtrung",

    __construct: function () {

        /*TITLE*/
        var imageCategories = $("#Categories a:first").text();
        this.imageTitle = Image.title;
        /*GET FROM TEMPLATE CONST*/
        this.imageCategories = imageCategories;

        /*LOAD TEMPLATE*/
        this.getTemplate();

        /*the*/
        $("<div id='mp3'/>").appendTo("#theImage");

        $("#baidu_voice_btn").remove().clone().prependTo("#theImage");

        this.bindElement();
        //$("#theImage")
    },

    /*BIND BUTTON*/
    bindElement: function () {

        $("#baidu_voice_btn label").on("click", function () {

            var btn = $(this).find("input");
            var value = btn.attr("data-code");

            console.log(value);

            Baidu.voiceConf.per = value;
            Baidu.baiduVoice();

            //return;
        })

        /*speed
         * */
        $("#speed a").on("click", function () {

            var obj = $(this);
            var type = obj.attr("data-dir");
            var spd = Baidu.voiceConf.spd;

            if (spd > 1 || spd < 10) {

                if (type == "dwn") {

                    spd--;
                }
                else spd++;

                Baidu.voiceConf.spd = spd;
                $("#speed_val").val(spd);

                /*VOICE*/
                Baidu.baiduVoice();
            }


            return;
        })
    },

    /*GET DETAIL*/
    getImageDetail: function () {

        $.get(TiengTrung.basehost+"/getImageDetail.php", {

            title: this.imageTitle,
            cat: this.imageCategories

        }, function (data) {

            console.log(data);

            TiengTrung.imageData = data;

            /*INIT DATA TO DISPLAY*/
            TiengTrung.init(data);

        });

        return;
    },

    /*INIT*/


    /*FIX CONTENT METHOD*/
    fixContent: function (content) {

        if (content.indexOf("。") != -1) {

            var pos = content.lastIndexOf("。");

            content = content.substr(0, pos + 1);

        }

        return content;

    },

    /*INIT FUNCTION*/
    init: function (data) {

        var title = TiengTrung.imageTitle;
        var cat = $.trim(TiengTrung.imageCategories);

        var conf = {};


        /*FIND CAT*/
        switch (cat) {
            case "旅游景点":
                conf = {

                    content: data["内容"],
                    tmpl: "#tour"
                };

                break;
            case "食品大全":

                conf = {

                    content: data["Title"] + ".准备原料." + data["Material"],
                    tmpl: "#food"
                };

                break;

            case "汉语成语":

                conf = {

                    content: "成语." + data["cyname"] + ".成语解释." + data["explain"],
                    tmpl: "#idiom"
                };

                //content = $.trim(data["explain"]);

                break;

            default:
                return;

        }

        /*FIX CONTENT OF PICTURE*/
        var content = $.trim(conf.content);
        var maximum = TiengTrung.textMaximum;


        //data[conf.content] = content;

        /*RENDER TO TEMPLATE*/
        var template = $(conf.tmpl).html();
        var text = Mustache.render(template, data);


        $("<div class='Tiengtrung'>").css("text-align", "left").html(text).appendTo("#theImage")

        /*READ - MORE*/
        TiengTrung.minimized(maximum);

        /*PREPARE FOR VOICE MP3*/
        content = TiengTrung.fixContent(content.substr(0, maximum));

        var source = TiengTrung.basehost+"/textToVoice.php?title=" + title + "&text=" + content;
        Baidu.voiceSource = source;

        Baidu.baiduVoice();

        return;

    },

    /*read more*/
    minimized: function (maximum) {


        var minimized_elements = $('#noidung');

        /*FIX NL2BR*/
        //minimized_elements
        minimized_elements.each(function () {
            var t = $(this).text();

            //console.log(t);

            if (t.length < maximum) return;

            $(this).html(
                t.slice(0, maximum) + '<span>... </span><a href="#" class="more">More</a>' +
                '<span style="display:none;">' + t.slice(maximum, t.length) + ' <a href="#" class="less">Less</a></span>'
            );

        });

        $('a.more', minimized_elements).click(function (event) {
            event.preventDefault();
            $(this).hide().prev().hide();
            $(this).next().show();
        });

        $('a.less', minimized_elements).click(function (event) {
            event.preventDefault();
            $(this).parent().hide().prev().show().prev().show();
        })

        return;
    },

   
    /*load template for image detail*/
    getTemplate: function () {

        $.get(TiengTrung.basehost+'/assets/template.html', function (data) {

            $("body").append(data);

        });

        return;
    }


};