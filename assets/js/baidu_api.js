/**
 * Created by donhatquang on 8/11/17.
 */

var Baidu = {

    /*PRIVATE*/
    voiceConf: {

        per: 0,
        spd: 4
    },

    voiceSource: "",
    myAudio: {},
    voiceCurrentContent: "", //current

    /*INIT*/
    __construct: function () {

        /*voice nhi nhanh*/
        this.voiceConf.per = 4;
        /*mp3*/
        this.myAudio = document.getElementById('mp3_audio');

        return;
    },

    /*PLAY AUDIO FROM TEXT*/
    baiduVoice: function () {

        //var source = source || this.voiceSource;

        var source = this.voiceSource + "&" + $.param(this.voiceConf);
        var dev = devmode || false;

        console.log("Baidu voice source: " + source);

        if (dev)
            source = "aidemo.mp3";

        //autoplay

        this.myAudio.src = source;

        return;
    },

    /*LOAD EVENT*/
    AudioLoaded: function () {

        console.log("Audio load finish");
        this.myAudio.play();
    },

    /*PLAY SOUND WHEN CLICK EVENT*/
    playSound: function (e) {

        var obj = $(e).parents(".chat_simi").find(".bubble");
        var myAudio = this.myAudio;

        /*FIND BUBLE TEXT*/
        var text = obj.text();

        console.log("sound:" + obj.hasClass("orange"));

        // var myAudio = document.getElementById('mp3_audio');

        // *AUDIO PLAYING*!/
        obj.addClass("orange speaking");

        // if (myAudio.duration > 0 && !myAudio.paused) {
        /*CHECK IF TOGGLE CURRENT AUDIO ELEMENT*/
        if (text == this.voiceCurrentContent) {

            if (myAudio.duration > 0 && !myAudio.paused) {

                /*PAUSE*/
                myAudio.pause();
                // this.endSound();

            }
            else myAudio.play();
        }

        /*NEW AUDIO CONTENT - DIFFERENT WITH CURRENT*/
        else {
            /*ADD NEW VOICE*/
            this.addVoice(text);
        }

        console.log("Play sound");

        return false;
    },

    endSound: function () {

        console.log("Finish sound");
        $(".speaking").removeClass("orange speaking");

        return;
    },

    /*VOICE SOURCE FOR SIMSIMI*/
    addVoice: function (content, lang) {

        var lang = lang || "zh";

        var source = "/textToVoice.php?title=simi&text=" + content + "&lang=" + lang;
        // console.log(source);

        this.voiceSource = source;
        this.voiceCurrentContent = content;

        Baidu.baiduVoice(source);


        return source;
    },

}